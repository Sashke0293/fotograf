<?php
/**
 * Step 1: Require the Slim Framework
 *
 * If you are not using Composer, you need to require the
 * Slim Framework and register its PSR-0 autoloader.
 *
 * If you are using Composer, you can skip this step.
 */

session_start();

require 'libs/meekrodb.2.3.class.php';
require 'libs/PHPMailer/class.phpmailer.php';
require 'libs/PHPMailer/class.smtp.php';
require 'fotograf.perm.ru/helpers/functions.php';
require 'fotograf.perm.ru/classes/user.php';

require 'libs/Slim/Slim.php';

\Slim\Slim::registerAutoloader();

/**
 * Step 2: Instantiate a Slim application
 *
 * This example instantiates a Slim application using
 * its default settings. However, you will usually configure
 * your Slim application now by passing an associative array
 * of setting names and values into the application constructor.
 */

require 'fotograf.perm.ru/views/fotoView.php';
require 'fotograf.perm.ru/models/fotoModel.php';

$app = new \Slim\Slim(array(
    'view' => new fotoView(),
    'debug' => true,
    'templates.path' => './fotograf.perm.ru/templates/layout'
));

$user = User::getInstance();

$app->hook('slim.before.dispatch', function () use ($app, $user) {
    $app->view->setData(array(
        'jsPath'=>'/assets/js/',
        'cssPath'=>'/assets/css/',
        'sassPath'=>'/assets/sass/',
        'imgPath'=>'/assets/img/',
        'photoPath'=>'/assets/photo/',
        'fontsPath'=>'/assets/fonts/',
        'bootstrap'=>'/assets/bootstrap/',
        'jquery'=>'/assets/jquery/',
        'user' => $user
    ));
});

setlocale ("LC_ALL", "ru_RU.utf8");
date_default_timezone_set('UTC');

/**
 * Step 3: Define the Slim application routes
 *
 * Here we define several Slim application routes that respond
 * to appropriate HTTP request methods. In this example, the second
 * argument for `Slim::get`, `Slim::post`, `Slim::put`, `Slim::patch`, and `Slim::delete`
 * is an anonymous function.
 */


// GET route
$app->get(
    '/',
    function () use ($app){
        $app->render('main.php',array(
            'modules' => array('head','slider','categories','webest','calendar'),
            'slider'  => DB::query('SELECT portfolio.img as img,portfolio.caption as caption,CONCAT(categories.alias,\'/\',portfolio.alias) as url FROM portfolio,categories WHERE slider<>0 AND category_id=categories.id ORDER BY slider DESC,portfolio.id DESC'),
            'categories'  => DB::query('SELECT id,img,img_blur,caption,alias as url FROM categories WHERE 1')
        ));
    }
);

$app->get(
    '/portfolio/:category',
    function ($category) use ($app){
        $app->render('main.php',array(
            'modules' => array('head','portfolio'),
            'category1' => DB::queryFirstRow('SELECT * FROM categories WHERE alias=%s',$category),
            'categories'  => DB::query('SELECT id,caption,alias as url FROM categories WHERE 1'),
        ));
    }
);

$app->get(
    '/portfolio/:category/:project',
    function ($category,$project) use ($app){
        $app->render('main.php',array(
            'modules' => array('head','project'),
            'project' => DB::queryFirstRow('SELECT * FROM portfolio WHERE alias=%s AND (SELECT COUNT(*) FROM portfolio_categories WHERE category_id=(SELECT id FROM categories WHERE alias=%s))',$project,$category),
            'categories'  => DB::query('SELECT id,caption,alias as url FROM categories WHERE 1')
        ));
    }
);

$app->get(
    '/about',
    function () use ($app){
        $app->render('main.php',array(
            'modules' => array('head','about'),
            'about'  => DB::query('SELECT * FROM about WHERE 1'),
            'categories'  => DB::query('SELECT id,caption,alias as url FROM categories WHERE 1')
        ));
    }
);

$app->get(
    '/feedback',
    function () use ($app){
        $app->render('main.php',array(
            'modules' => array('head','feedback'),
            'categories'  => DB::query('SELECT id,caption,alias as url FROM categories WHERE 1')
        ));
    }
);

$app->get(
    '/price',
    function () use ($app){
        $app->render('main.php',array(
            'modules' => array('head','price'),
            'price'  => DB::query('SELECT type,caption,description,time,price FROM price WHERE 1'),
            'categories'  => DB::query('SELECT id,caption,alias as url FROM categories WHERE 1')
        ));
    }
);

$app->post(
    '/',
    function () use ($app){
        $app->redirect('/super/settings');

    }
);

//            echo md5(md5("19skynet84")."NaCl");

/*$app->get(
    '/categories/:category',
    function ($category) use ($app){
        $app->render('category.php');
    }
);

$app->get(
    '/categories/:category/:project',
    function ($category,$project) use ($app){
        $app->render('project.php');
    }
);*/

$app->get(
    '/super',
    function () use ($app, $user){
        if($user->login){
            $app->render('admin.php',array(
                "modules" => array("modules")
            ));
        }else{
            $app->redirect('/super/settings');
        }
    }
);
$app->get(
    '/super/galery',
    function () use ($app, $user){

        if($user->login){
            $app->render('admin.php',array(
                "modules" => array("galery")
            ));
        }else{
            $app->redirect('/super/settings');
        }
    }
);
$app->get(
    '/super/projects',
    function () use ($app, $user){

        if($user->login){
            $app->render('admin.php',array(
                "modules" => array("projects")
            ));
        }else{
            $app->redirect('/super/projects');
        }
    }
);
$app->get(
    '/super/settings',
    function () use ($app, $user){
            $app->render('admin.php',array(
                "modules" => array("settings")
            ));
    }
);
$app->get(
    '/super/calendar',
    function () use ($app, $user){

        if($user->login){

            $app->render('admin.php',array(
                "modules" => array("calendar")
            ));

        }else{
            $app->redirect('/super/settings');
        }
    }
);
$app->get(
    '/super/fields',
    function () use ($app, $user){

        if($user->login){

            $app->render('admin.php',array(
                "modules" => array("fields")
            ));

        }else{
            $app->redirect('/super/settings');
        }
    }
);
$app->get(
    '/super/price',
    function () use ($app, $user){

        if($user->login){

            $app->render('admin.php',array(
                "modules" => array("price")
            ));

        }else{
            $app->redirect('/super/settings');
        }
    }
);
$app->get(
    '/super/about',
    function () use ($app, $user){

        if($user->login){

            $app->render('admin.php',array(
                "modules" => array("about")
            ));

        }else{
            $app->redirect('/super/settings');
        }
    }
);
$app->get(
    '/super/categories',
    function () use ($app, $user){

        if($user->login){

            $app->render('admin.php',array(
                "modules" => array("categories")
            ));

        }else{
            $app->redirect('/super/settings');
        }
    }
);
$app->get(
    '/super/images',
    function () use ($app, $user){

        if($user->login){

            $app->render('admin.php',array(
                "modules" => array("images")
            ));

        }else{
            $app->redirect('/super/settings');
        }
    }
);

// POST route
$app->post(
    '/ajax',
    function () use ($app, $user) {

        $app->renderRaw('ajax.php');
    }
);

// POST route
$app->post(
    '/lazySubmit',
    function () use ($app, $user) {

        $app->renderRaw('ajax.php');
        $app->redirect($_SERVER['HTTP_REFERER']?$_SERVER['HTTP_REFERER']:'/super/projects');

    }
);

// PUT route
$app->put(
    '/put',
    function () {
        echo 'This is a PUT route';
    }
);

// PATCH route
$app->patch('/patch', function () {
    echo 'This is a PATCH route';
});

// DELETE route
$app->delete(
    '/delete',
    function () {
        echo 'This is a DELETE route';
    }
);

/**
 * Step 4: Run the Slim application
 *
 * This method should be called last. This executes the Slim application
 * and returns the HTTP response to the HTTP client.
 */
$app->run();
