/* AjaxUploader.setState({

 step: с какой картинки грузить,
 container: куда грузить картинки,
 action: какую функцию ajax использовать
 debug: выводит в консоль инфу
 });
 *
 *
 *
 *
 * */
// AjaxUploader.run(); запустит загрузки изображений

AjaxUploader = (function(){

    var step = 0; // начало
    var table = 'portfolio'; // начало
    var _field = 'category_id'; //поле в бд
    var _value = '1'; //значение поля
    var count = 1; // кол-во загружаемых картинок больше 1 лучше пока не делать
    var container = '.ajax_categories'; // контейнер в который загружаются картинки
    var action = 'upload_imgs'; // функция которая будет вызвана в ajax
    var add_data = {}; // дополнительные параметры для расширения
    var debug = true;
    var path = 'portfolio/thumb/';
    var template = '/fotograf.perm.ru/helpers/tmpl/categories.php';
    var cols = [4,4,6];
    var _class = 'vh50';


    function get_template(){
        $.post(template ,{},function(data){
            template = data;
        });
        return template;
    }

    function upload_loop(step){

        $.post('/ajax', {'action':action, 'data':{'step':step, 'count': count, 'table':table, 'field':_field, 'value':_value}}, function(data){

            if(debug){
                console.log(data);
            }

            data = JSON.parse(data);
            if(data.stop){

                $(container).append(template);

                $(container).find('[data-class]').last().attr('class','col-lg-'+cols[0]+' col-md-'+cols[1]+' col-sm-'+cols[2]+' '+_class);
                $(container).find('[data-img]').last().attr('src','<?php echo $photoPath; ?>'+path+data.img).attr('alt',data.alt).attr('title',data.title);
                //$(container).find('[data-img]').last().css('background-image','url(<?php echo $photoPath; ?>'+path+data.img+')');
                $(container).find('[data-heading]').last().text(data.caption);
                $(container).find('[data-description]').last().text(data.description);
                $(container).find('[data-project-link]').last().attr('href',data.link);

                upload_loop(data.step);
            }else{

                var timer = 0;
                $(".project_img_container").each(function () {
                    $(this).delay(timer).promise().done(function () {
                        $(this).fadeIn(500);
                    });
                    timer += 200;
                });
            }
        });
    }

    return {
        getState: function() {
            console.log(step);
            console.log(container);
            console.log(action);
            console.log(add_data);
            console.log(debug);
            console.log(table);
            console.log(path);
            console.log(cols);
        },
        setState: function(options) {
            step = options.step || step;
            container = options.container || container;
            action = options.action || action;
            add_data = options.add_data || add_data;
            debug = options.debug || debug;
            table = options.table || table;
            path = options.path || path;
            cols = options.cols || cols;
            template = options.template || template;
            _class = options._class || _class;
            _field = options._field || _field;
            _value = options._value || _value;
            return true;
        },
        run: function() {
            $.post(template ,{},function(data){
                template = data;
                upload_loop(step);
            });
            return true;
        }
    };
})();