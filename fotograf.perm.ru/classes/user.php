<?php
/**
 * Created by PhpStorm.
 * User: sasha
 * Date: 06.09.15
 * Time: 1:53
 */


class User{

    public static $login = false;

    protected static $_instance;

    private function __construct(){

        if(isset($_POST['auth'])){

            $usr = $_POST['auth'];

            unset($_SESSION['user']);

            $confirm = DB::queryFirstRow("SELECT * FROM users WHERE login=%s AND password=%s;", $usr['login'], md5(md5($usr['password'])."NaCl"));

            if(!!($confirm)){ // user detected
                $this->login = true;
                $_SESSION['user'] = $confirm;
            }else{
                $this->login = false;
            }

        }else if(isset($_SESSION['user'])){

            $usr = $_SESSION['user'];

            unset($_SESSION['user']);

            $confirm = DB::queryFirstRow("SELECT * FROM users WHERE login=%s AND password=%s;", $usr['login'], $usr['password']);

            if(!!($confirm)){ // user detected
                $this->login = true;
                $_SESSION['user'] = $confirm;
            }else{
                $this->login = false;
            }

        }else{

            unset($_SESSION['user']);

            $this->login = false;

        }

    }

    private function __clone(){
    }

    public static function getInstance() {

        if (null === self::$_instance) {

            self::$_instance = new self();
        }

        return self::$_instance;
    }
}