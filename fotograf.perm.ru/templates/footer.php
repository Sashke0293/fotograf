<script src="<?php echo $jsPath; ?>scrollr.js"></script>
<script src="<?php echo $jsPath; ?>sasha.js"></script>

<svg class="likeaninja" version="1.1" xmlns="http://www.w3.org/2000/svg">
	<filter id="blur">
		<feGaussianBlur stdDeviation="20" />
	</filter>
</svg>

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter33564577 = new Ya.Metrika({
                    id:33564577,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/33564577" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<noindex><script async src="//track.smmscan.ru/?id=MGZhM2IwY2M0YTc5M2VjYjc5MDE1MmFmY2NhMzMxMGZ8NTkz"></script></noindex> 

</body>
</html>