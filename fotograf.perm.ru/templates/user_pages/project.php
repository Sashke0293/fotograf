<div class="container-fliud h100 scroll-normal" id="block-view">
    <div class="row h100">
        <div id="block-prev" class="col-lg-4 col-md-5 col-sm-12 col-xs-12 h100 fixed bg-white">
            <div class="tbl no-p no-b project-descr-block">
                <div class="tbl-row">
                    <div class="tbl-cell p-l-110 p-r-100 bb-1 p-b-60 o-hidden">
                        <h1 class="usans-tcaps cgray f70 lhl text-left m-r--100">
                            <?= $project['caption'] ?>
                        </h1>
                    </div>
                </div>
                <div class="tbl-row">
                    <div class="tbl-cell p-b-20 project-descr-text">
                        <p class="text-justify p-r-100 m-b-20 p-t-20 cgray">
                            <?= $project['description_full'] ?>
                        </p>
                        <a href="javascript:window.history.back()" class="arrow-btn usans-tcaps">
                            Назад
                        </a>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-8 col-md-7 col-sm-12 col-xs-12 pull-right bg-white">

            <div class="container-fluid">
                <div class="row ajax_projects">

                </div>
            </div>

        </div>
    </div>
</div>

<script>

    AjaxUploader.setState({'table':'photo', 'path':'photo/orig/', 'cols':[6,6,12],'container':'.ajax_projects',template:'/fotograf.perm.ru/helpers/tmpl/projects.php','_field':'portfolio_id','_value':<?= $project['id'] ?>});
    AjaxUploader.run();

    var body = $(document.body);
    var getMyDiv = function(myImg){
        var orig = $(myImg).parent().parent();

        var div = $('<div class="leftblock project_close"></div>');
        var img = $('<div class="spread bgcontain">');
        //img.css('background-image', $(myImg).css('background-image'));
        img.css('background-image', 'url('+$(myImg).attr('src')+')');
        var left = $('<a class="leftblock-left" href="#"><div class="mainslider-arrow-left"></div></a>');
        var right = $('<a class="leftblock-right" href="#"><div class="mainslider-arrow-right"></div></a>');
        div.append(img);
        div.append(left);
        div.append(right);

        div.click(function(){
            div.removeClass('view');
            setTimeout(function(){
                div.remove();
            },2000);
        });

        left.click(function(e){
            var prev = orig.prev();
            if(!prev.length){
                prev = orig.parent().children().last();
            }

            var nimg = prev.find('.project_img')[0];
            var ndiv = getMyDiv(nimg);
            div.before(ndiv);
            
            setTimeout(function(){
                ndiv.addClass('view');
                div.removeClass('view');
                setTimeout(function(){
                    div.remove();
                },2000);
            },100);

            e.preventDefault();
            e.stopPropagation();
        });

        right.click(function(e){
            var next = orig.next();
            if(!next.length){
                next = orig.parent().children().first();
            }

            var nimg = next.find('.project_img')[0];
            var ndiv = getMyDiv(nimg);
            div.after(ndiv);
            
            setTimeout(function(){
                ndiv.addClass('view');
                div.removeClass('view');
                setTimeout(function(){
                    div.remove();
                },2000);
            },100);

            e.preventDefault();
            e.stopPropagation();
        });

        var ox;
        div.on('touchstart',function(e){
            ox=e.originalEvent.touches[0].clientX;
        });
        div.on('touchend',function(e){
            if(ox - 30 > e.originalEvent.changedTouches[0].clientX){
                right.click();
            }else if(ox + 30 < e.originalEvent.changedTouches[0].clientX){
                left.click();
            }
        });
        
        return div;
    }

    $(document).on('click','.project_img',function(event){
        var div = getMyDiv(this);

        body.append(div);

        setTimeout(function(){
            div.addClass('view');
        },100);

        event.preventDefault();
    });

</script>