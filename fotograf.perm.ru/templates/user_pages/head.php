<div class="scroll-normal loader">
	<div class="bg">
		<div class="sh"></div>
		<div class="fg"></div>
	</div>
	<div class="pc bhs45 f50"><span>0</span>%</div>
</div>
<div class="head top-shadow clearfix">
    <h3 class="bhs45 pull-left f35 m-t-11">
        <a class="white link-flat" href="/">
        	<span class="fotograf-icon hover-icon f50 valign-top">&#128247;</span>
        	<span class="valign-top">fotograf.perm.ru</span>
        </a>
    </h3>
    <div class="pull-right m-t-10">
        <a class="menu-open djvu-xlight white f30 link-flat link-linethrough" href="#">меню</a>
    </div>
</div>
<div class="scroll-normal menu">
	<div class="fg usans-tcaps f25 white">
		<a class="menu-close" href="#"></a>
		<p>
			<a class="white link-flat link-linethrough lh1" href="/">Главная</a>
		</p>
		<? foreach($categories as $category){ ?>
		<p>
			<a class="white link-flat link-linethrough lh1 category-show" data-id="<?= $category['id'] ?>" href="/portfolio/<?= $category['url'] ?>"><?= $category['caption'] ?></a>
		</p>
		<? } ?>
		<p>
			<a class="white link-flat link-linethrough lh1" href="/about">О нас</a>
		</p>
		<p>
			<a class="white link-flat link-linethrough lh1" href="/feedback">Отзывы</a>
		</p>
		<p>
			<a class="white link-flat link-linethrough lh1" href="/price">Услуги и цены</a>
		</p>

		<form class="menuform" method="POST" action="/ajax">
			<p>Свяжитесь с нами</p>
			<input id="menuform-name" class="f20" type="text" name="data[name]" placeholder="Имя" required>
			<label for="menuform-name" class="fotograf-icon">&#20154;</label>
			<input id="menuform-phone" class="f20" type="tel" name="data[phone]" placeholder="Телефон" required>
			<label for="menuform-phone" class="fotograf-icon">&#9990;</label>

			<input type="hidden" name="action" value="addEventFlat">

			<button>
				Отправить
				<span class="menuform-arrow-right"></span>
			</button>
		</form>

		<p>
			+7 (922) <span class="f30">332-40-20</span>
		</p>
		<p class="darkgrey opensans f16">
			<a class="gray link-flat color-transition p-r-2em" target="_blank" href="http://vk.com/fotograf_perm_ru">Вконтакте</a>
			<a class="gray link-flat color-transition p-r-2em" target="_blank" href="http://facebook.com/fotograf.perm">Facebook</a>
			<a class="gray link-flat color-transition p-r-2em" target="_blank" href="http://twitter.com/fotograf_perm">Twitter</a>
			<a class="gray link-flat color-transition p-r-2em" target="_blank" href="http://instagram.com/fotograf.perm.ru">Instagram</a>
		</p>
	</div>
</div>