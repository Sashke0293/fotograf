<div class="block webest-block text-center<?php if($nv==1) echo 'view stop" id="block-view' ?>">
	<h1 class="usans-tcaps gray f70 m-t-2em m-b-1em p-lr-1em h-linethrough inblock">
		<?= DB::queryFirstField('SELECT value FROM fields WHERE field="webetter_title"') ?>
	</h1>
	<div class="opensans darkgray f15 text-justify columns inblock w70 bg-transwhite shadow-transwhite">
		<?php
		mb_internal_encoding('UTF-8');
		$val = DB::queryFirstField('SELECT value FROM fields WHERE field="webetter"');
		$val = '<span class="usans-tcaps f70 lh1 inblock pull-left m-r-0_25em">'.mb_substr($val,0,1).'</span>'.mb_substr($val,1);
		$val = '<p>'.$val.'</p>';
		$val = preg_replace("#\n\s*\n\s*\n#", "</p><p>", $val);
		$val = preg_replace("#\n#", "<br>", $val);
		echo $val;
		?>
	</div>
	<div class="m-b-40vw m-t-30 text-center inblock w70"><?php
		for ($i=1; $i <= 5; $i++) { 
			$num = DB::queryFirstField('SELECT value FROM fields WHERE field=%s','webetter_num'.$i);
			$text = DB::queryFirstField('SELECT value FROM fields WHERE field=%s','webetter_text'.$i);

			if(strlen($num) > 3){
				$num = substr_replace($num,'&#8203;',2,0);
			}

			$text = preg_replace("#\n#", "<br>", $text);

			?><div class="inblock relative webest-icon valign-top">
			<div class="usans-tcaps dark text-center webest-icon-number"><?= $num ?></div>
			<p class="opensans f15 marinegray m-t-16rem"><?= $text ?></p>
			</div><?php
		}
	?></div>
	<?php /*<div class="mainslider-arrow-bot fotograf-icon f70 pink"></div>*/ ?>
</div>