<div class="container-fliud h100 scroll-normal" id="block-view">
	<div class="row">
		<h1 class="usans-tcaps gray f70 m-t-2em m-b-1em h-linethrough price-header">Оставьте отзыв</h1>
	</div>
	<div class="price-block-left price-block-right">
		<p>Мои дорогие и любимые клиенты, я невероятно благодарна вам за тёплые и добрые слова, за потраченное вами время на написание отзыва моей работе, за то, что делитесь вашими впечатлениями. Ваше мнение очень важно и ценно для меня и моих будущих клиентов, поэтому не стесняйтесь делиться вашим опытом.</p>
		<div id="vk_comments" style="margin:0 auto;"></div>
		<script type="text/javascript">
		VK.Widgets.Comments("vk_comments", {limit: 20, width: "1000", attach: "*"});
		</script>
	</div>
</div>