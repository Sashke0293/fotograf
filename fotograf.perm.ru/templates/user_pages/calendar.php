<div class="block calendar-block<?php if($nv==1) echo 'view stop" id="block-view' ?>">
	<h1 class="usans-tcaps gray f70 m-t-2em h-linethrough inblock">
		Календарь
	</h1>
	<p class="opensans gray f18 m-t-1em">
		<?= DB::queryFirstField('SELECT value FROM fields WHERE field=%s','calendar_subtitle'); ?>
	</p>
	<div class="clearfix p-r-40pc p-r-0-xs relative gray">
		<?php
			$now = time();
			require('parts/calendar.php');
		?>
		<?php /*<div class="opensans darkgray f15 m-t-50 p-50 pull-left">
			<table class="maincalendar-legend">
				<tr>
					<td colspan="2" class="f18 p-b-1em">
						Условные обозначения:
					</td>
				</tr>
				<tr>
					<td class="usans-tcaps gray f25">
						38
					</td>
					<td class="p-lr-1em">
						свободный день
					</td>
				</tr>
				<tr>
					<td class="usans-tcaps gray f25 relative day_pna">
						38
					</td>
					<td class="p-lr-1em">
						некоторые часы заняты
					</td>
				</tr>
				<tr>
					<td class="usans-tcaps gray f25 relative day_na">
						38
					</td>
					<td class="p-lr-1em">
						день занят
					</td>
				</tr>
				<tr>
					<td class="usans-tcaps gray f25 relative text-center day_s">
						38
					</td>
					<td class="p-lr-1em">
						день выбран
					</td>
				</tr>
			</table>
		</div>*/ ?>
	</div>
	<div class="clearfix m-t-2em">
		<form class="mainform opensans gray f25 m-t-20 pull-left" method="POST" action="/ajax">
			<div>
				<label>
					Привет, меня зовут
					<input name="data[name]" type="text" required>
					.
				</label>
				<label>
					<?php /*Свяжитесь со мной*/ ?>
					Меня интересует фотосъёмка
					<input id="mainform-date" name="data[date]" type="text" required>.
					Свяжитесь со мной.
				</label>
				<input id="mainform-date-unix" name="data[date_unix]" type="hidden">
				<?php /*<label>
					в
					<input id="mainform-time" name="data[time]" type="tel" required>
					.
				</label>*/ ?>
			</div>
			<div>
				<label>
					Мой телефон для связи +7			
					<input id="mainform-phone" name="data[phone]" type="tel" required>
				</label>
				<button class="usans-tcaps f30">
					Отправить
					<span class="mainform-arrow-right"></span>
				</button>
			</div>
			<input type="hidden" name="action" value="addEvent">
		</form>
		<div class="darkgray opensans f15 m-t-20 p-l-30 p-l-0-xs pull-left bg-transwhite shadow-transwhite">
			<p>
				<strong>Фотограф:</strong>
			</p>
			<p>Мельникова Анна Дмитриевна</p>
			<p class="m-t-2em">
				<strong>Свяжитесь с нами:</strong>
			</p>
			<p>+7 (922)-332-40-20</p>
			<p class="darkgray">
				<a class="gray link-flat color-transition" href="mailto:fotografperm@yandex.ru">fotografperm@yandex.ru</a>
			</p>
			<p class="f16 darkgray">
				<a class="gray link-flat color-transition p-r-2em" target="_blank" href="http://vk.com/fotograf_perm_ru">Вконтакте</a>
				<a class="gray link-flat color-transition p-r-2em" target="_blank" href="http://facebook.com/fotograf.perm">Facebook</a>
				<a class="gray link-flat color-transition p-r-2em" target="_blank" href="http://twitter.com/fotograf_perm">Twitter</a>
				<a class="gray link-flat color-transition p-r-2em" target="_blank" href="http://instagram.com/fotograf.perm.ru">Instagram</a>
			</p>
		</div>
	</div>
</div>