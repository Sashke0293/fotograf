<?php foreach ($about as $ak => $av): ?>
<?php 
	$pull = ($ak%2 != 1)?'pull-left':'pull-right';
	$pulr = ($ak%2 == 1)?'pull-left':'pull-right';
?>
<div class="block about-block <?php if($nv==1 && $ak==0) echo 'view stop" id="block-view' ?>">
	<div class="bg <?= $pulr ?>" style="background-image:url(/assets/photo/about/<?= $av['img'] ?>)"></div>
	<h1 class="usans-tcaps gray f70 <?= $pull ?> m-t-2em m-b-1em h-linethrough"><?= $av['caption'] ?></h1>
	<div class="opensans darkgrey f15 <?= $pull ?> text-justify">
		<p><?= preg_replace("#\n#", "<br>",$av['description']) ?></p>
	</div>
</div>
<?php endforeach; ?>
<?php /*<div class="about-block scroll-normal <?php if($nv==1) echo 'view stop" id="block-view' ?>">
	<div class="bg pull-right">
		<?php foreach ($about as $ak => $av): ?>
		<?php $text = preg_replace("#\n#", "<br>", $av['description']); ?>
		<?php if($ak % 2 != 1): ?>
		<div class="row f70 m-t-2em">
			<div class="col-xs-3 f15 p-lr-1em">
				<img src="/assets/photo/about/<?= $av['img'] ?>" width="100%">
			</div>
			<div class="col-xs-9 opensans darkgrey f15 pull-left text-justify p-lr-1em">
				<p><?= $text ?></p>
			</div>
		</div>
		<?php else: ?>
		<div class="row f30 m-t-2em">
			<div class="col-xs-9 opensans darkgrey f15 pull-left text-justify p-lr-1em">
				<p><?= $text ?></p>
			</div>
			<div class="col-xs-3 f15 p-lr-1em">
				<img src="/assets/photo/about/<?= $av['img'] ?>" width="100%">
			</div>
		</div>
		<?php endif; ?>
		<?php endforeach; ?>
	</div>
	<h1 class="usans-tcaps gray f70 pull-left m-t-2em m-b-1em h-linethrough">О нас</h1>
	<div class="opensans darkgrey f15 pull-left text-justify">
		<?php
		$val = DB::queryFirstField('SELECT value FROM fields WHERE field="about"');
		$val = '<p>'.$val.'</p>';
		$val = preg_replace("#\n\s*\n\s*\n#", "</p><p>", $val);
		$val = preg_replace("#\n#", "<br>", $val);
		echo $val;
		?>
	</div>
	<div class="bg bg-double">
		<?php foreach ($about as $ak => $av): ?>
		<?php $text = preg_replace("#\n#", "<br>", $av['description']); ?>
		<?php if($ak % 2 != 1): ?>
		<div class="row f70 m-t-2em">
			<div class="col-xs-3 f15 p-lr-1em">
				<img src="/assets/photo/about/<?= $av['img'] ?>" width="100%">
			</div>
			<div class="col-xs-9 opensans darkgrey f15 pull-left text-justify p-lr-1em">
				<p><?= $text ?></p>
			</div>
		</div>
		<?php else: ?>
		<div class="row f30 m-t-2em">
			<div class="col-xs-9 opensans darkgrey f15 pull-left text-justify p-lr-1em">
				<p><?= $text ?></p>
			</div>
			<div class="col-xs-3 f15 p-lr-1em">
				<img src="/assets/photo/about/<?= $av['img'] ?>" width="100%">
			</div>
		</div>
		<?php endif; ?>
		<?php endforeach; ?>
	</div>
</div> */ ?>
<?php /*<div class="block about-block">
	<div class="bg pull-left" style="background-image:url(/assets/photo/about/2.png)"></div>
	<h1 class="usans-tcaps gray f70 pull-right m-t-2em m-b-1em h-linethrough">&nbsp;</h1>
	<div class="opensans darkgrey f15 pull-right text-justify">
		<p>Text</p>
		<p>Text</p>
		<p>Text</p>
	</div>
</div>
<div class="block about-block">
	<div class="bg pull-right" style="background-image:url(/assets/photo/about/3.png)"></div>
	<h1 class="usans-tcaps gray f70 pull-left m-t-2em m-b-1em h-linethrough">А также</h1>
	<div class="opensans darkgrey f15 pull-left text-justify">
		<p>Text</p>
		<p>Text</p>
		<p>Text</p>
	</div>
</div>*/ ?>