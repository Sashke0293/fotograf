<?php
$cert_show = ('1' == DB::queryFirstField('SELECT value FROM fields WHERE field="promo_slide"'));

if($cert_show){
	$cert_img = '/assets/img/'.DB::queryFirstField('SELECT value FROM images WHERE field="cert"');
}else{
	$cert_img = '/assets/photo/portfolio/orig/'.$slider[0]['img'];
}
?>
<div class="block slider-block<?php if($nv==1) echo ' view stop" id="block-view' ?>">
	<ul class="mainslider bhs45 f70 lh1_5 " style="background-image:url(<?= $cert_img ?>);">
		<?php
		$slide_last = array_pop($slider);
		array_unshift($slider, $slide_last);

		foreach($slider as $k => $slide):
		if($cert_show && $k==1): ?>
		<li class="view">
			<div class="bg" style="background-image:url(<?= $cert_img ?>);"></div>
			<div class="fg">
				<a class="white link-flat" href="/price">
					Сертификат на фотосессию - отличный подарок!
				</a>
			</div>
		</li>
		<?php endif; ?>
		<li<? if(!$cert_show && $k==1) echo ' class="view"' ?>>
			<div class="bg" style="background-image:url(/assets/photo/portfolio/orig/<?= $slide['img'] ?>);"></div>
			<div class="fg">
				<a class="white link-flat" href="/portfolio/<?= $slide['url'] ?>">
					<?= $slide['caption'] ?>
				</a>
			</div>
		</li>
		<?php endforeach; ?>
	</ul>
	<div class="mainslider-arrow-left"></div>
	<div class="mainslider-arrow-right"></div>
	<div class="mainslider-arrow-bot<?php /* fotograf-icon f70 white*/ ?>"></div>
</div>