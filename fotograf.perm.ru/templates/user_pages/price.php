<div class="container-fliud h100 scroll-normal" id="block-view">
	<div class="row">
		<h1 class="usans-tcaps gray f70 m-t-2em m-b-1em h-linethrough price-header">Услуги и цены</h1>
	</div>
	<div class="row middark">
		<div class="col-md-6 col-sm-12 col-xs-12 price-block-left">
			<?php
			$colors = array('indigo','magenta','orange');
			for ($i=0; $i < 3; $i++) { 
			$cur = $price[$i];
			$text = preg_replace("#\n\s*\n#", "</p><p>", $cur['description']);
			$color = $colors[$i];
			?>
			<table class="price-table m-b-10rem">
				<tr>
					<td rowspan="3" class="fotograf-icon f55 valign-top <?= $color ?>">&#xe60<?= $i ?>;</td>
					<td colspan="2" class="usans-tcaps f30">
						<label class="price-request-init" for="price-request-cb<?= $i+1 ?>">
							<?= $cur['type'] ?>
							<strong class="usans-hcaps"><?= $cur['caption'] ?></strong>
						</label>
					</td>
				</tr>
				<tr>
					<td colspan="2" class="opensans f18">
						<p><?= $text ?></p>
					</td>
				</tr>
				<tr>
					<td class="usans-tcaps f25">
						<span class="fotograf-icon f18 valign-middle">&#x231b;</span>
						<?= $cur['time'] ?>
					</td>
					<td class="usans-tcaps f25">
						<span class="fotograf-icon f18 valign-middle">&#xfe69;</span>
						<?= $cur['price'] ?>
					</td>
				</tr>
			</table>
			<?php }	?>
			<?php /*<table class="price-table m-b-10rem">
				<tr>
					<td rowspan="3" class="fotograf-icon f55 valign-top indigo">&#xe600;</td>
					<td colspan="2" class="usans-tcaps f25">
						Фотопакет
						<strong class="usans-hcaps">Promo</strong>
					</td>
				</tr>
				<tr>
					<td colspan="2" class="opensans f15">
						<p>Вы получаете 50 фотографий с цветокоррекцией и эффектами.</p>
						<p>10-15 фотографии будут в обработке, а именно вы будете иметь идеальную кожу, яркие глаза и чудесные волосы на портретных фото. Возможны коррекции фигуры.</p>
					</td>
				</tr>
				<tr>
					<td class="usans-tcaps f25">
						<span class="fotograf-icon f18 valign-middle">&#x231b;</span>
						5-7 дней
					</td>
					<td class="usans-tcaps f25">
						<span class="fotograf-icon f18 valign-middle">&#xfe69;</span>
						3 000 рублей
					</td>
				</tr>
			</table>
			<table class="price-table m-b-10rem">
				<tr>
					<td rowspan="3" class="fotograf-icon f55 valign-top magenta">&#xe601;</td>
					<td colspan="2" class="usans-tcaps f25">
						Фотопакет
						<strong class="usans-hcaps">Ultra</strong>
					</td>
				</tr>
				<tr>
					<td colspan="2" class="opensans f15">
						<p>Вы получаете 100 фотографий с цветокоррекцией, а также с частичной ретушью в обработке.</p>
						<p>20-30 фотографий будут художественно отретушированы, а именно вы будете иметь идеальную кожу, яркие глаза и чудесные волосы. Коррекции фигуры, одежды.</p>
					</td>
				</tr>
				<tr>
					<td class="usans-tcaps f25">
						<span class="fotograf-icon f18 valign-middle">&#x231b;</span>
						7-14 дней
					</td>
					<td class="usans-tcaps f25">
						<span class="fotograf-icon f18 valign-middle">&#xfe69;</span>
						6 000 рублей
					</td>
				</tr>
			</table>
			<table class="price-table m-b-10rem">
				<tr>
					<td rowspan="3" class="fotograf-icon f55 valign-top orange">&#xe602;</td>
					<td colspan="2" class="usans-tcaps f25">
						Фотопакет
						<strong class="usans-hcaps">Premium</strong>
					</td>
				</tr>
				<tr>
					<td colspan="2" class="opensans f15">
						<p>Выбирая эту услугу, вы получаете от 100-150 фотографий.</p>
						<p>Из них 25-45 фотографий с художественной ретушью.</p>
					</td>
				</tr>
				<tr>
					<td class="usans-tcaps f25">
						<span class="fotograf-icon f18 valign-middle">&#x231b;</span>
						14-25 дней
					</td>
					<td class="usans-tcaps f25">
						<span class="fotograf-icon f18 valign-middle">&#xfe69;</span>
						9 000 рублей
					</td>
				</tr>
			</table>*/ ?>
		</div>
		<div class="col-md-6 col-sm-12 col-xs-12 price-block-right">
			<?php
			$colors = array('marineblue','purple','orangeface');
			for ($i=3; $i < 6; $i++) { 
			$cur = $price[$i];
			$text = preg_replace("#\n\s*\n#", "</p><p>", $cur['description']);
			$color = $colors[$i-3];
			?>
			<table class="price-table m-b-7_5rem">
				<tr>
					<td rowspan="3" class="fotograf-icon f55 valign-top <?= $color ?>">&#x<?= ($i==5)?'23f0':'e603' ?>;</td>
					<td class="usans-tcaps f30">
						<label class="price-request-init" for="price-request-cb<?= $i+1 ?>">
							<?= $cur['type'] ?>
							<strong class="usans-hcaps"><?= $cur['caption'] ?></strong>
						</label>
					</td>
				</tr>
				<tr>
					<td class="opensans f18">
						<p><?= $text ?></p>
					</td>
				</tr>
				<tr>
					<td class="usans-tcaps f25">
						<span class="fotograf-icon f18 valign-middle">&#xfe69;</span>
						<?= $cur['price'] ?>
					</td>
				</tr>
			</table>
			<?php }	?>
			<?php /*<table class="price-table m-b-10rem">
				<tr>
					<td rowspan="3" class="fotograf-icon f55 valign-top marineblue">&#xe603;</td>
					<td class="usans-tcaps f25">
						Сертификат на фотосессию
						<strong class="usans-hcaps">1 час</strong>
					</td>
				</tr>
				<tr>
					<td class="opensans f15">
						<p>Вы можете подарить сертификат своим близким, или использовать его сами в удобное время.</p>
					</td>
				</tr>
				<tr>
					<td class="usans-tcaps f25">
						<span class="fotograf-icon f18 valign-middle">&#xfe69;</span>
						3 000 рублей
					</td>
				</tr>
			</table>
			<table class="price-table m-b-7_5rem">
				<tr>
					<td rowspan="3" class="fotograf-icon f55 valign-top purple">&#xe603;</td>
					<td class="usans-tcaps f25">
						Сертификат на фотосессию
						<strong class="usans-hcaps">2 часа</strong>
					</td>
				</tr>
				<tr>
					<td class="opensans f15">
						<p>Вы можете подарить сертификат своим близким, или использовать его сами в удобное время.</p>
					</td>
				</tr>
				<tr>
					<td class="usans-tcaps f25">
						<span class="fotograf-icon f18 valign-middle">&#xfe69;</span>
						5 000 рублей
					</td>
				</tr>
			</table>
			<table class="price-table m-b-7_5rem">
				<tr>
					<td rowspan="3" class="fotograf-icon f55 valign-top purple">&#x23f0;</td>
					<td class="usans-tcaps f25">
						Почасовая и свадебная съёмка
					</td>
				</tr>
				<tr>
					<td class="opensans f15">
						<p>Услуга включает в себя выезд фотографа, цветокоррекцию и работу с эффектами, а также ретушь.</p>
					</td>
				</tr>
				<tr>
					<td class="usans-tcaps f25">
						<span class="fotograf-icon f18 valign-middle">&#xfe69;</span>
						3 000 рублей - 1 час
					</td>
				</tr>
			</table>*/ ?>
			<div class="price-attention fotograf-icon m-b-7_5rem">
				<h3 class="usans-hcaps f30 m-t-1em m-b-0_5em">Внимание!</h3>
				<p class="opensans f18">Услуги аренды студии (при студийной<br>съёмке) оплачиваются клиентом.</p>
			</div>
		</div>
	</div>

<form class="priceform" action="/ajax" method="POST">
	<input type="radio" name="data[selection]" class="price-request-cb" id="price-request-hide" checked>
	<input type="radio" name="data[selection]" class="price-request-cb" id="price-request-cb1" value="<?= $price[0]['type'].' '.$price[0]['caption'] ?>">
	<input type="radio" name="data[selection]" class="price-request-cb" id="price-request-cb2" value="<?= $price[1]['type'].' '.$price[1]['caption'] ?>">
	<input type="radio" name="data[selection]" class="price-request-cb" id="price-request-cb3" value="<?= $price[2]['type'].' '.$price[2]['caption'] ?>">
	<input type="radio" name="data[selection]" class="price-request-cb" id="price-request-cb4" value="<?= $price[3]['type'].' '.$price[3]['caption'] ?>">
	<input type="radio" name="data[selection]" class="price-request-cb" id="price-request-cb5" value="<?= $price[4]['type'].' '.$price[4]['caption'] ?>">
	<input type="radio" name="data[selection]" class="price-request-cb" id="price-request-cb6" value="<?= $price[5]['type'].' '.$price[5]['caption'] ?>">

	<input type="radio" name="data[stage]" id="price-request-stage1" checked>
	<input type="radio" name="data[stage]" id="price-request-stage2">
	<input type="radio" name="data[stage]" id="price-request-stage3">

	<input type="submit" id="price-request-submit">
	<input type="hidden" name="action" value="addEvent">

	<div class="price-request-form middark">
		<label for="price-request-hide" class="price-request-hide"></label>
		<div class="price-request-block">
			<div class="price-request-stage-number usans-tcaps f27 text-center">
				<label for="price-request-stage1">1</label>
				<label for="price-request-stage2">2</label>
				<label for="price-request-stage3">3</label>
			</div>

			<div class="price-request-stage1">
				<h1 class="usans-tcaps f55 no-m text-center">Убедитесь в выборе</h1>
				<p class="opensans f15 no-m text-center">Убедитесь, что вы выбрали правильный фотопакет или сертификат</p>
				<label class="price-request-back usans-hcaps f27" for="price-request-hide">
					<div>Закрыть</div>
				</label>
				<label class="price-request-next usans-hcaps f27" for="price-request-stage2">
					<div>Дальше</div>
				</label>
<?php
$colors = array('indigo','magenta','orange','marineblue','purple','orangeface');
foreach ($price as $i => $cur):
$text = preg_replace("#\n\s*\n#", "</p><p>", $cur['description']);
$color = $colors[$i];
?>
<?php if($i < 3): ?>
			<table class="price-table m-b-10rem price-request-item<?= $i+1 ?>">
				<tr>
					<td rowspan="3" class="fotograf-icon f55 valign-top <?= $color ?>">&#xe60<?= $i ?>;</td>
					<td colspan="2" class="usans-tcaps f30">
						<?= $cur['type'] ?>
						<strong class="usans-hcaps"><?= $cur['caption'] ?></strong>
					</td>
				</tr>
				<tr>
					<td colspan="2" class="opensans f18">
						<p><?= $text ?></p>
					</td>
				</tr>
				<tr>
					<td class="usans-tcaps f25">
						<span class="fotograf-icon f18 valign-middle">&#x231b;</span>
						<?= $cur['time'] ?>
					</td>
					<td class="usans-tcaps f25">
						<span class="fotograf-icon f18 valign-middle">&#xfe69;</span>
						<?= $cur['price'] ?>
					</td>
				</tr>
			</table>
<?php else: ?>
			<table class="price-table m-b-7_5rem price-request-item<?= $i+1 ?>">
				<tr>
					<td rowspan="3" class="fotograf-icon f55 valign-top <?= $color ?>">&#x<?= ($i==5)?'23f0':'e603' ?>;</td>
					<td class="usans-tcaps f30">
						<?= $cur['type'] ?>
						<strong class="usans-hcaps"><?= $cur['caption'] ?></strong>
					</td>
				</tr>
				<tr>
					<td class="opensans f18">
						<p><?= $text ?></p>
					</td>
				</tr>
				<tr>
					<td class="usans-tcaps f25">
						<span class="fotograf-icon f18 valign-middle">&#xfe69;</span>
						<?= $cur['price'] ?>
					</td>
				</tr>
			</table>
<?php endif; ?>
<?php endforeach; ?>
			</div>
			<div class="price-request-stage2">
				<h1 class="usans-tcaps f55 no-m text-center">Календарь</h1>
				<p class="opensans f15 no-m text-center">Выберите подходящий для вас день встречи</p>
				<label class="price-request-back usans-hcaps f27" for="price-request-stage1">
					<div>Назад</div>
				</label>
				<label class="price-request-next usans-hcaps f27" for="price-request-stage3">
					<div>Дальше</div>
				</label>

				<?php
					$now = time();
					require('parts/calendar.php');
				?>
			</div>
			<div class="price-request-stage3">
				<h1 class="usans-tcaps f55 no-m text-center">Форма заявки</h1>
				<p class="opensans f15 no-m text-center">Заполните форму заявки, мы вам перезвоним для подтверждения</p>
				<label class="price-request-back usans-hcaps f27" for="price-request-stage2">
					<div>Назад</div>
				</label>
				<label class="price-request-next usans-hcaps f27" for="price-request-submit">
					<div>Отправить заявку</div>
				</label>

				<div class="mainform opensans f18 m-t-20">
					<div>
						<label>
							Привет, меня зовут
							<input name="data[name]" type="text" required>
							.
						</label>
						<label>
							<?php /*Свяжитесь со мной*/ ?>
							Меня интересует фотосъёмка
							<input id="priceform-date" name="data[date]" type="text" required>.
							Свяжитесь со мной.
						</label>
						<input id="priceform-date-unix" name="data[date_unix]" type="hidden">
						<?php /*<label>
							в
							<input id="priceform-time" name="data[time]" type="tel" required>
							.
						</label>*/ ?>
					</div>
					<div>
						<label>
							Мой телефон для связи +7			
							<input id="priceform-phone" name="data[phone]" type="tel" required>
						</label>
						<?php /*<button class="usans-tcaps f30">
							Отправить
							<span class="mainform-arrow-right"></span>
						</button>*/ ?>
					</div>
					<?php /*<input type="hidden" name="action" value="addEvent">*/ ?>
				</form>
			</div>
		</div>
	</div>
</form>
</div>