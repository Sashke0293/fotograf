<?php
$now=strtotime('today',$now);
$month_start = strtotime('first day of this month midnight',$now);

if(date('w',$month_start) !== '1'){
	$start_date = strtotime('last Monday of last month',$now);
}else{
	$start_date = $month_start;
}
$end_date = strtotime('first Monday of next month',$now);

$month = date('n',$now);

$booked = DB::queryFirstColumn('SELECT UNIX_TIMESTAMP(start) FROM events WHERE start>%s AND start<%s AND class="event-warning"',date('Y-m-d H:i:s',$start_date),date('Y-m-d H:i:s',$end_date));
foreach ($booked as &$value) {
	$value = strtotime('today',$value);
}

$sel0 = $now-24*60*60;
while(in_array($sel0, $booked)){
	echo '<!-- 0: '.$sel0.'//-->';
	$sel0-=24*60*60;
}
$sel1 = $now;
while(in_array($sel1, $booked)){
	echo '<!-- 1: '.$sel1.'//-->';
	$sel1+=24*60*60;
}
$sel2 = $sel1+24*60*60;
while(in_array($sel2, $booked)){
	echo '<!-- 2: '.$sel2.'//-->';
	$sel2+=24*60*60;
}

$halfbooked = DB::queryFirstColumn('SELECT UNIX_TIMESTAMP(start) FROM events WHERE start>%s AND start<%s AND class="event-info"',date('Y-m-d H:i:s',$start_date),date('Y-m-d H:i:s',$end_date));
foreach ($halfbooked as &$value) {
	$value = strtotime('today',$value);
}

$t_day_free = DB::queryFirstField('SELECT value FROM fields WHERE field=%s','calendar_free');
$t_day_part = DB::queryFirstField('SELECT value FROM fields WHERE field=%s','calendar_part');
$t_day_na = DB::queryFirstField('SELECT value FROM fields WHERE field=%s','calendar_na');

?>
<table class="maincalendar usans-tcaps f25 m-t-50 pull-left text-center" data-month="<?= $month_start ?>">
	<tr class="f30 lh1">
		<td class="month_p" colspan="2"><?php echo strftime('%b', strtotime('last month',$now)) ?></td>
		<td colspan="3"><?php echo strftime('%B',$now) ?></td>
		<td class="month_n" colspan="2"><?php echo strftime('%b', strtotime('next month',$now)) ?></td>
	</tr>
	<tr class="f30 lh1">
		<td class="m_day_p" colspan="2"><?php echo strftime('%d', $sel0) ?></td>
		<td class="m_day_c" colspan="3"><?php echo strftime('%d',$sel1) ?></td>
		<td class="m_day_n" colspan="2"><?php echo strftime('%d', $sel2) ?></td>
	</tr>
	<tr>
		<td>пн</td>
		<td>вт</td>
		<td>ср</td>
		<td>чт</td>
		<td>пт</td>
		<td>сб</td>
		<td>вс</td>
	</tr>
	<?php
	echo '<!--'.date('w',$month_start).'//-->';
	$count = 0;
	for($cur = $start_date ; $cur < $end_date ; $cur += 60*60*24 ){
		if($count % 7 == 0){
			echo '<tr>';
		}

		if(in_array($cur,$booked)){
			$cl = ' day_na';
			$title = $t_day_na;
		}else if(in_array($cur,$halfbooked)){
			$cl = ' day_pna';
			$title = $t_day_part;
		}else{
			$cl = '';
			$title = $t_day_free;
		}

		$m = intval(date('n',$cur));

		switch($m-$month){
			case -1:
			case 11:
				$cl = ' class="day_p'.$cl.'"'; break;
			case 0:
				if(empty($cl)){
					$cl = '';
				}else{
					$cl = ' class="' . $cl . '"';
				}
				break;
			default:
				$cl = ' class="day_n'.$cl.'"';
		}

		$odf = date('j',$cur);

		if($cl===''){
			$cl = ' data-day="' . $odf . '"';
		}

		echo '<td title="'. $title .'"'. $cl . '>' . $odf . '</td>';

		$count++;

		if($count % 7 == 0){
			echo '</tr>';
		}
	}
	?>
</table>