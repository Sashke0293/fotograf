<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="Description" content="Обращаясь к фотографу, чтобы запечатлеть события своей жизни, клиенты требуют гарантий того, что кадры окажутся на уровне и развернуто покажут события дня и настроение героев съемки. Клиенты, заказывающие фотосъемку, оценивают снимок по искренним эмоциям и естественным позам, трогательным улыбкам">
    <meta name="Keywords" content="фотограф пермь, фотосессия пермь, свадебный фотограф, семейный фотограф, фотограф на свадьбу пермь, детский фотограф пермь, love story">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="shortcut icon" type="image/png" href="/assets/img/favicon.png">
    <title>Fotograf.perm.ru - Лучшие фотографы Перми работают для Вас</title>
    <link rel="stylesheet" href="<?php echo $bootstrap; ?>css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo $bootstrap; ?>bower_components/bootstrap-calendar/css/calendar.min.css"> <!-- for admin -->
    <link rel="stylesheet" href="<?php echo $jquery; ?>jquery.fancybox.css">
    <link rel="stylesheet" href="<?php echo $jquery; ?>jquery-ui-1.11.4/jquery-ui.min.css">
    <link rel="stylesheet" href="<?php echo $cssPath; ?>fonts.css">
    <link rel="stylesheet" href="<?php echo $cssPath; ?>root.css">
    <link rel="stylesheet" href="<?php echo $cssPath; ?>sasha.css">
    <link rel="stylesheet" href="<?php echo $cssPath; ?>denis.css">
    <link rel="stylesheet" href="<?php echo $sassPath; ?>media.css">
    <link rel="stylesheet" href="<?php echo $cssPath; ?>sweetalert.css">

    <script src="<?php echo $jquery; ?>jquery-1.11.3.min.js"></script>
    <script src="<?php echo $bootstrap; ?>js/bootstrap.min.js"></script>
    <script src="<?php echo $bootstrap; ?>js/ru-RU.js"></script> <!-- for admin -->
    <script src="<?php echo $bootstrap; ?>js/underscore.js"></script> <!-- for admin -->
    <script src="<?php echo $bootstrap; ?>js/calendar.js"></script> <!-- for admin -->
    <script src="<?php echo $jquery; ?>jquery.fancybox.js"></script>
    <script src="<?php echo $jquery; ?>jquery.mousewheel.min.js"></script>
    <script src="<?php echo $jquery; ?>jquery-ui-1.11.4/jquery-ui.min.js"></script>
    <script src="<?php echo $jquery; ?>jquery.form.js"></script>
    <script src="<?php echo $jquery; ?>jquery.maskedinput.min.js"></script>
    <script src="<?php echo $jsPath; ?>moment.js"></script>
    <script src="<?php echo $jsPath; ?>sweetalert.min.js"></script>
    <?php /*<script src="<?php echo $jsPath; ?>timepicki.js"></script>*/ ?>
    <script>
        <?php
            include_once "fotograf.perm.ru/helpers/ajaxUploader.js.php";
        ?>
    </script>
    <script src="<?php echo $jsPath; ?>sasha.js"></script>
    <script src="<?php echo $jsPath; ?>mainPage.js"></script>

    <script type="text/javascript" src="//vk.com/js/api/openapi.js?121"></script>

    <script type="text/javascript">
    VK.init({apiId: 5246488, onlyWidgets: true});
    </script>
</head>
<body>