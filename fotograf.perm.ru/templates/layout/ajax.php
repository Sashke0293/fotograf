<?php
/**
 * Created by PhpStorm.
 * User: sasha
 * Date: 06.09.15
 * Time: 14:25
 */

    (isset($_POST['action']))? // если задана функция
        (isset($_POST['data']))? // если заданы параметры
            Ajax::$_POST['action']($_POST['data']): // вызываем функцию с параметрами
            Ajax::$_POST['action'](): // вызываем функцию без параметров
        ""; // написал а че не понял =)

abstract class Ajax{
    public static function saveProject($data){

        global $user;

        if($user->login){

            $target_dir = $_SERVER['DOCUMENT_ROOT']."/assets/photo/portfolio/orig/";
            $filenm = preg_replace('/[^a-zA-Z0-9\.\-]/', '_', time().'_'.$_FILES["data"]["name"]['img']);
            $filenm = basename($filenm);
            $target_file = $target_dir . $filenm;
            $uploadOk = 1;
            $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

            // Check if file already exists
            if (file_exists($target_file)) {
                echo "Sorry, file already exists.";
                $uploadOk = 0;
            }
            // Allow certain file formats
            if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                && $imageFileType != "gif" ) {
                echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                $uploadOk = 0;
            }
            // Check if $uploadOk is set to 0 by an error
            if ($uploadOk == 0) {
                echo "Sorry, your file was not uploaded.";
                // if everything is ok, try to upload file
            } else {
                if (move_uploaded_file($_FILES["data"]["tmp_name"]["img"], $target_file)) {
                    echo "The file ". basename( $_FILES["data"]["name"]["img"]). " has been uploaded.";
                } else {
                    echo "Sorry, there was an error uploading your file.";
                }
            }

            if(!isset($data['category_id']) || gettype($data['category_id'])!=="array"){
                $data['category_id']=array(0);
            }

            if(!empty($_FILES["data"]["name"]['img'])){
                DB::query("UPDATE portfolio SET slider=%s, category_id=%s, caption=%s, description=%s, description_full=%s, img=%s, alias=%s WHERE id=%s", $data['slider'], $data['category_id'][0], $data['caption'], $data['description'], $data['description_full'], $filenm, $data['alias'], $data['id']);
            }else{
                DB::query("UPDATE portfolio SET slider=%s, category_id=%s, caption=%s, description=%s, description_full=%s, alias=%s WHERE id=%s", $data['slider'], $data['category_id'][0], $data['caption'], $data['description'], $data['description_full'], $data['alias'], $data['id']);
            }

            DB::delete('portfolio_categories','portfolio_id=%d',$data['id']);

            $cats=array();
            foreach ($data['category_id'] as $v) {
                $cats[]=array(
                    'category_id'=>$v,
                    'portfolio_id'=>$data['id']
                );
            }
            DB::insert('portfolio_categories',$cats);

            if(!empty($_FILES["prgimg"]["name"][0])){
                foreach($_FILES["prgimg"]['name'] as $key => $filenm){
                    $filenm = preg_replace('/[^a-zA-Z0-9\.\-]/', '_', $filenm);
                    $filenm = time().'_'.$filenm;
                    DB::query("INSERT INTO photo (`portfolio_id`,`img`) VALUES (%s, %s)", $data['id'], $filenm);

                    $target_dir = $_SERVER['DOCUMENT_ROOT']."/assets/photo/photo/orig/";
                    $target_file = $target_dir . basename($filenm);
                    move_uploaded_file($_FILES["prgimg"]['tmp_name'][$key], $target_file);
                }
            }
        }

    }

    public static function saveCategory($data){

        global $user;

        if($user->login){
            $fnm=!empty($_FILES["data"]["name"]['img'])?"img":"img_blur";

            $target_dir = $_SERVER['DOCUMENT_ROOT']."/assets/photo/category/";
            $filenm = preg_replace('/[^a-zA-Z0-9\.\-]/', '_', time().'_'.$_FILES["data"]["name"][$fnm]);
            $filenm = basename($filenm);
            $target_file = $target_dir . $filenm;
            $uploadOk = 1;
            $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

            // Check if file already exists
            if (file_exists($target_file)) {
                echo "Sorry, file already exists.";
                $uploadOk = 0;
            }
            // Allow certain file formats
            if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                && $imageFileType != "gif" ) {
                echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                $uploadOk = 0;
            }
            // Check if $uploadOk is set to 0 by an error
            if ($uploadOk == 0) {
                echo "Sorry, your file was not uploaded.";
                // if everything is ok, try to upload file
            } else {
                if (move_uploaded_file($_FILES["data"]["tmp_name"][$fnm], $target_file)) {
                    echo "The file ". basename( $_FILES["data"]["name"][$fnm]). " has been uploaded.";
                } else {
                    echo "Sorry, there was an error uploading your file.";
                }
            }

            if(!empty($_FILES["data"]["name"][$fnm])){
                DB::query("UPDATE categories SET %b=%s, caption=%s,alias=%s WHERE id=%d",$fnm,$filenm,$data['caption'],$data['alias'],$data['id']);
            }else{
                DB::query("UPDATE categories SET caption=%s,alias=%s WHERE id=%d",$data['caption'],$data['alias'],$data['id']);
            }
        }
    }

    public static function saveAbout($data){

        global $user;

        if($user->login){
            $fnm="img";

            $target_dir = $_SERVER['DOCUMENT_ROOT']."/assets/photo/about/";
            $filenm = preg_replace('/[^a-zA-Z0-9\.\-]/', '_', time().'_'.$_FILES["data"]["name"][$fnm]);
            $filenm = basename($filenm);
            $target_file = $target_dir . $filenm;
            $uploadOk = 1;
            $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

            // Check if file already exists
            if (file_exists($target_file)) {
                echo "Sorry, file already exists.";
                $uploadOk = 0;
            }
            // Allow certain file formats
            if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                && $imageFileType != "gif" ) {
                echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                $uploadOk = 0;
            }
            // Check if $uploadOk is set to 0 by an error
            if ($uploadOk == 0) {
                echo "Sorry, your file was not uploaded.";
                // if everything is ok, try to upload file
            } else {
                if (move_uploaded_file($_FILES["data"]["tmp_name"][$fnm], $target_file)) {
                    echo "The file ". basename( $_FILES["data"]["name"][$fnm]). " has been uploaded.";
                } else {
                    echo "Sorry, there was an error uploading your file.";
                }
            }

            if(!empty($_FILES["data"]["name"][$fnm])){
                DB::query("UPDATE about SET %b=%s, description=%s, caption=%s WHERE id=%d",$fnm,$filenm,$data['description'],$data['caption'],$data['id']);
            }else{
                DB::query("UPDATE about SET description=%s, caption=%s WHERE id=%d",$data['description'],$data['caption'],$data['id']);
            }
        }
    }

    public static function saveImages($data){

        global $user;

        if($user->login){
            $fnm="img";

            $target_dir = $_SERVER['DOCUMENT_ROOT']."/assets/img/";
            $filenm = preg_replace('/[^a-zA-Z0-9\.\-]/', '_', time().'_'.$_FILES["data"]["name"][$fnm]);
            $filenm = basename($filenm);
            $target_file = $target_dir . $filenm;
            $uploadOk = 1;
            $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

            // Check if file already exists
            if (file_exists($target_file)) {
                echo "Sorry, file already exists.";
                $uploadOk = 0;
            }
            // Allow certain file formats
            if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                && $imageFileType != "gif" ) {
                echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                $uploadOk = 0;
            }
            // Check if $uploadOk is set to 0 by an error
            if ($uploadOk == 0) {
                echo "Sorry, your file was not uploaded.";
                // if everything is ok, try to upload file
            } else {
                if (move_uploaded_file($_FILES["data"]["tmp_name"][$fnm], $target_file)) {
                    echo "The file ". basename( $_FILES["data"]["name"][$fnm]). " has been uploaded.";
                } else {
                    echo "Sorry, there was an error uploading your file.";
                }
            }

            if(!empty($_FILES["data"]["name"][$fnm])){
                DB::query("UPDATE images SET value=%s WHERE id=%d",$filenm,$data['id']);
            }else{
                die('WTF?');
            }
        }
    }

    public static function delete($data){

        global $user;

        if($user->login){
            $ok = DB::query("DELETE FROM ".$data['table']." WHERE id=%s", $data['id']);
            if(!!$ok){
                echo json_encode(array(
                    'status' => 'success'
                ));
            }else{
                echo json_encode(array(
                    'status' => 'error'
                ));
            }
        }
    }
    public static function addProject(){

        global $user;

        if($user->login) {
            DB::query("INSERT INTO portfolio (`caption`) VALUES ('Пустой проект')");
        }
    }

    public static function addCategory(){

        global $user;

        if($user->login) {
            DB::query("INSERT INTO categories (`caption`) VALUES ('Новая категория')");
        }
    }

    public static function addAbout(){

        global $user;

        if($user->login) {
            DB::query("INSERT INTO about (`description`) VALUES ('Новый блок')");
        }
    }

    public static function upload_imgs($data){
        $limit = $data['step'];
        $count = $data['count'];
        $table = $data['table'];
        $field = $data['field'];
        $value = $data['value'];
        if($table==='portfolio' && $field==='category_id'){
            $confirm = DB::queryFirstRow("SELECT img,caption,description,alias,portfolio_categories.category_id as category_id FROM portfolio,portfolio_categories WHERE portfolio_categories.category_id=%s AND portfolio_categories.portfolio_id=portfolio.id LIMIT %d,%d",$value,$limit,$count);
        }else{
            $confirm = DB::queryFirstRow("SELECT * FROM ".$table." WHERE %b=%s LIMIT %d,%d",$field,$value,$limit, $count);
        }
        $limit++;
        if(!!$confirm){
            if(!isset($confirm['alias'])){
                $confirm['link'] = '#';
            }else if(isset($confirm['category_id'])){
                $confirm['link'] = '/portfolio/'.DB::queryFirstField('SELECT alias FROM categories WHERE id=%s',$confirm['category_id']).'/'.$confirm['alias'];
            }else{
                $confirm['link'] = '/portfolio/'.$confirm['alias'];
            }
            if(!isset($confirm['caption'])){
                $confirm['caption']='';
            }
            if(!isset($confirm['description'])){
                $confirm['description']='';
            }
            if(!isset($confirm['alt'])){
                $confirm['alt'] = '';
            }
            if(!isset($confirm['title'])){
                $confirm['title'] = '';
            }
            echo json_encode(array(
                "stop" => true,
                "img" => $confirm['img'],
                "caption" => $confirm['caption'],
                "description" =>$confirm['description'],
                "link" =>$confirm['link'],
                "alt" => $confirm['alt'],
                "title" => $confirm['title'],
                "step" => $limit
            ));
        }else{
            echo json_encode(array(
                "step" => $limit,
                "stop" => false,
                "confirm" => $confirm,
                "tmp" => array($value,$limit,$count)
            ));
        }
    }

    public static function addEvent($data){
        $res = array();

        if(isset($data['date'])){
            $date = strtotime($data['date'].date('.Y 00:00:00'));
            if(!$date){
                $date = strtotime($data['date']);
            }
        }else{
            $date = $data['date_unix'];
        }
        
        if(!empty($date)){
            if(isset($data['time'])){
                $time = strtotime($data['time'],$date);
            }else{
                $time = strtotime('12:00',$date);
            }

            if(!empty($time)){
                $start=date('Y-m-d H:i:s',$time);
                $end=date('Y-m-d H:i:s',$time + 60*60);

                $s_date=date('d.m.Y',$time);
                $s_time=date('H:i',$time);

                $title  = 'Привет, меня зовут '.$data['name'];
                if(isset($data['selection'])){
                    $title .= '. Меня интересует '.$data['selection'];
                }
                $title .= '. Свяжитесь со мной '.$s_date.'. Мой телефон для связи +7'.$data['phone'];

                DB::insert('events',array('title'=>$title,'url'=>'','class'=>'event-default','start'=>$start,'end'=>$end));

                $mail = new PHPMailer();

                $mail->isSMTP();
                $mail->SMTPAuth=true;
                $mail->Host='localhost';
                $mail->Username='noreply@annamelnikova.ru';
                $mail->Password='KuTxrWyx';

                $mail->CharSet='UTF-8';
                $mail->setFrom('noreply@annamelnikova.ru','Fotograf Perm');
                $mail->addAddress('fotografperm@yandex.ru');
                $mail->Subject='Новая заявка с сайта';
                $mail->Body='<p>'.$title.'</p>';
                $mail->isHTML(true);
                $mail->AltBody=$title;
                if($mail->send()){
                    $res['success']=1;
                }else{
                    $res['error']=array(187,'phpmailer error',$mail->ErrorInfo);
                }
            }else{
                $res['error']=array(100,'wrong time format');
            }
        }else{
            $res['error']=array(100,'wrong date format');
        }

        echo json_encode($res);
    }

    public static function addEventFlat($data){
        $res = array();

        if(!empty($data['name']) && !empty($data['phone'])){
            $title='Привет, меня зовут '.$data['name'].'. Свяжитесь со мной как можно скорее. Мой телефон для связи '.$data['phone'];

            $now = date('Y-m-d H:i:s',strtotime('today 07:00:00'));
            $nowend = date('Y-m-d H:i:s',strtotime('today 19:00:00'));

            DB::insert('events',array('title'=>$title,'url'=>'','class'=>'event-default','start'=>$now,'end'=>$nowend));

            $mail = new PHPMailer();

            $mail->isSMTP();
            $mail->SMTPAuth=true;
            $mail->Host='localhost';
            $mail->Username='noreply@annamelnikova.ru';
            $mail->Password='KuTxrWyx';

            $mail->CharSet='UTF-8';
            $mail->setFrom('noreply@annamelnikova.ru','Fotograf Perm');
            $mail->addAddress('fotografperm@yandex.ru');
            $mail->Subject='Новая заявка с сайта';
            $mail->Body='<p>'.$title.'</p>';
            $mail->isHTML(true);
            $mail->AltBody=$title;
            if($mail->send()){
                $res['success']=1;
            }else{
                $res['error']=array(187,'phpmailer error',$mail->ErrorInfo);
            }
        }else{
            $res['error']=array(101,'empty data');
        }     

        echo json_encode($res);
    }

    public static function addLockEvent($data){
        $res = array();

        global $user;

        if($user->login){
            $date = strtotime($data['date'].' 09:00:00');
            if(!$date){
                $date = strtotime($data['date']) + 60*60;
            }

            if(!empty($date)){
                $start=date('Y-m-d H:i:s',$date);

                $flag = (!isset($data['part'])||$data['part']=="false");

                $end=date('Y-m-d H:i:s',$date + (($flag)?(12*60*60):(60*60)));

                if(isset($data['title']) && !empty($data['title'])){
                    $title = $data['title'];
                }else{
                    $title=($flag)?'День занят':'День частично занят';
                }

                $class=($flag)?'event-warning':'event-info';

                DB::insert('events',array('title'=>$title,'url'=>'','class'=>$class,'start'=>$start,'end'=>$end));

                $res['success']=1;
            }else{
                $res['error']=array(100,'wrong date format');
            }
        }else{
            $res['error']='not logged';
        }

        echo json_encode($res);
    }

    public static function getCalendar($now){
        require('fotograf.perm.ru/templates/user_pages/parts/calendar.php');
    }

    public static function swapPhoto($data){
        $res = array();

        global $user;

        if($user->login){
            $cat1 = DB::queryFirstField('SELECT portfolio_id FROM photo WHERE id=%d',$data[0]);
            $cat2 = DB::queryFirstField('SELECT portfolio_id FROM photo WHERE id=%d',$data[1]);

            if($cat1 == $cat2){
                $d1 = $data[0];
                $d2 = $data[1];
                if($d1>$d2){
                    $d=$d1;
                    $d1=$d2;
                    $d2=$d;
                    $dir=-1;
                }else{
                    $dir=1;
                }

                $imgs = DB::query('SELECT * FROM photo WHERE portfolio_id=%d AND id>=%d and id<=%d',$cat1,$d1,$d2);

                if($dir==-1){
                    $first = count($imgs)-1;
                    $last = 0;
                }else{
                    $last = count($imgs)-1;
                    $first = 0;
                }

                $tmp = $imgs[$first]['id'];

                for ($k=$first;$k!=$last;$k+=$dir) {
                    $imgs[$k]['id'] = $imgs[$k+$dir]['id'];
                }

                $imgs[$last]['id'] = $tmp;

                DB::replace('photo',$imgs);

                $res['success']=1;
            }else{
                $res['error']='not match';
            }
        }else{
            $res['error']='not logged';
        }

        echo json_encode($res);
    }

    public static function saveFields($data){
        $res = array();

        global $user;

        if($user->login){
            $id = $data['id'];
            $text = $data['text'];

            DB::update('fields',array('value'=>$text), "id=%d",$id);

            $res['success']=1;
        }else{
            $res['error']='not logged';
        }

        echo json_encode($res);
    }

    public static function savePrice($data){
        $res = array();

        global $user;

        if($user->login){
            $id = $data['id'];

            DB::update('price',array(
                                    'type'=>$data['type'],
                                    'caption'=>$data['caption'],
                                    'description'=>$data['description'],
                                    'time'=>$data['time'],
                                    'price'=>$data['price'],
                                ), "id=%d",$id);

            $res['success']=1;
        }else{
            $res['error']='not logged';
        }

        echo json_encode($res);
    }

    public static function updatePhotoAlt($data){
        $res = array();

        global $user;

        if($user->login){
            $id = $data['id'];

            DB::update('photo',array(
                                    'alt'=>$data['alt'],
                                    'title'=>$data['title'],
                                ), "id=%d",$id);

            $res['success']=1;
        }else{
            $res['error']='not logged';
        }

        echo json_encode($res);
    }
}