<?php

$prices = DB::query('SELECT * FROM price');

foreach ($prices as $price) {
	?>

	<form class="row" method="POST" action="/lazySubmit">
		<div class="col-md-3">
			<p>
				Тип
				<input type="text" name="data[type]" class="admin_input" value="<?php echo $price['type']; ?>">
			</p>
			<p>
				Название
				<input type="text" name="data[caption]" class="admin_input" value="<?php echo $price['caption']; ?>">
			</p>
			<p>
				Время
				<input type="text" name="data[time]" class="admin_input" value="<?php echo $price['time']; ?>">
			</p>
			<p>
				Цена
				<input type="text" name="data[price]" class="admin_input" value="<?php echo $price['price']; ?>">
			</p>
			<button>Сохранить</button>
		</div>
		<div class="col-md-9">
			<textarea name="data[description]" style="width:100%;height:200px;resize:vertical;"><?= $price['description'] ?></textarea>
			<input name="data[id]" type="hidden" value="<?= $price['id'] ?>">
			<input name="action" type="hidden" value="savePrice">
		</div>
	</form>

	<?php
}

?>