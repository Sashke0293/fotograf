<?php

    $abts = DB::query("SELECT * FROM about ORDER BY id ASC");
?>

<div class="row">
    <div class="col-md-3 col-md-offset-9 text-right">
        <p></p>
        <button class="btn btn-success addBlock">Добавить блок</button>
    </div>
</div>
<div class="row">
    <?php foreach($abts as $abt): ?>
        <div class="col-md-12">
            <form action="/lazySubmit" method="post" enctype="multipart/form-data" class="well equal">
                <div class="media">
                    <div class="media-left">
                        <button type="button" class="clear-btn" onclick="$(this).find('.admin_file')[0].click();">
                            <img class="media-object" src="<?php echo $photoPath."about/".$abt['img']; ?>" width="300">
                            <input type="file" name="data[img]" class="admin_file hidden">
                        </button>
                    </div>
                    <div class="media-body">
                        Заголовок <input type="text" name="data[caption]" class="admin_input" value="<?php echo $abt['caption']; ?>"><p></p>
                        Текст <textarea class="admin_textarea" name="data[description]" rows="7"><?php echo $abt['description']; ?></textarea>
                    </div>
                </div>
                <input type="hidden" name="data[id]" value="<?php echo $abt['id']; ?>">
                <input type="hidden" name="action" value="saveAbout">
                <button type="submit" class="btn btn-success" data-trigger="hover" data-toggle="popover" data-placement="bottom" data-container="body" data-content="Сохранить измения текста в данном блоке?">Сохранить</button>
                <button type="submit" data-id="<?php echo $abt['id']; ?>" class="btn btn-warning deleteBlock">Удалить блок</button>
            </form>

        </div>


    <?php endforeach; ?>
</div>
<script>
    $(".admin_file").change(function () {
        $(this).parent().parent().parent().parent().submit();
    });
    $(function () {
        $('[data-toggle="popover"]').popover()
    });
    $('.deleteBlock').click(function (e) {
        e.preventDefault();
        id = $(this).attr('data-id');
        $.post('/ajax',{'action':"delete", data:{'id':id,'table':'about'}}, function (data) {
            location.reload();
        });
    });
    $('.addBlock').click(function (e) {
        e.preventDefault();
        $.post('/ajax',{'action':"addAbout"}, function (data) {
            location.reload();
        });
    });

</script>