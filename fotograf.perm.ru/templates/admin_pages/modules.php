<div class="row p-b-30">
<?php /*    <div class="col-md-3 equal">
        <a href="/super/galery" class="sas-hover-block">
            <div class="sas-hover-text">Галерея</div>
        </a>
        <img src="<?php echo $imgPath; ?>galery-fon.jpg" width="100%" alt="">
    </div> */ ?>
    <div class="col-md-8 equal">
        <a href="/super/projects" class="sas-hover-block">
            <div class="sas-hover-text">Проекты</div>
        </a>
        <img src="<?php echo $imgPath; ?>projects-fon.jpg" width="100%" alt="">
    </div>
    <div class="col-md-4 equal text-center">
        <a href="/super/calendar" class="sas-hover-block">
        </a>
        <div class="sas-calendar-day">
            <?php echo date("d"); ?>
        </div>
        <div class="sas-calendar-lay">
            <?php
            switch (date("l")) {
                case "Monday":
                    echo "Понедельник";
                    break;
                case "Tuesday":
                    echo "Вторник";
                    break;
                case "Wednesday":
                    echo "Среда";
                    break;
                case "Thursday":
                    echo "Четверг";
                    break;
                case "Friday":
                    echo "Пятница";
                    break;
                case "Saturday":
                    echo "Суббота";
                    break;
                case "Sunday":
                    echo "Воскресенье";
                    break;
                default:
                    break;
            }
            ?>
        </div>
    </div>
</div><!-- /.row -->