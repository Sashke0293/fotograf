<?php

$fields = DB::query('SELECT * FROM fields');

foreach ($fields as $field) {
	?>

	<form class="row" method="POST" action="/lazySubmit">
		<div class="col-md-2">
			<p><?= $field['field'] ?></p>
			<button>Сохранить</button>
		</div>
		<div class="col-md-10">
			<textarea name="data[text]" style="width:100%;height:<?= ($field['big'])?400:80 ?>px;resize:vertical;"><?= $field['value'] ?></textarea>
			<input name="data[id]" type="hidden" value="<?= $field['id'] ?>">
			<input name="action" type="hidden" value="saveFields">
		</div>
	</form>

	<?php
}

?>