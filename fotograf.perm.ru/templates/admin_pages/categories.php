<?php

    $cats = DB::query("SELECT * FROM categories ORDER BY id DESC");
?>

<div class="row">
    <div class="col-md-3 col-md-offset-9 text-right">
        <p></p>
        <button class="btn btn-success addCategory">Добавить категорию</button>
    </div>
</div>
<div class="row">
    <?php foreach($cats as $cat): ?>
        <div class="col-md-6">
            <form action="/lazySubmit" method="post" enctype="multipart/form-data" class="well equal">
                <div class="media">
                    <div class="media-left">
                        <button type="button" class="clear-btn" onclick="$(this).find('.admin_file')[0].click();">
                            <img class="media-object" src="<?php echo $photoPath."category/".$cat['img']; ?>" width="300">
                            <input type="file" name="data[img]" class="admin_file hidden">
                        </button>
                        <button type="button" class="clear-btn" onclick="$(this).find('.admin_file')[0].click();">
                            <img class="media-object" src="<?php echo $photoPath."category/".$cat['img_blur']; ?>" width="300">
                            <input type="file" name="data[img_blur]" class="admin_file hidden">
                        </button>
                    </div>
                    <div class="media-body">
                        Название <h4 class="media-heading"><input type="text" name="data[caption]" class="admin_input" value="<?php echo $cat['caption']; ?>"></h4>
                        Алиас <input type="text" name="data[alias]" class="admin_input" value="<?php echo $cat['alias']; ?>"><p></p>
                    </div>
                </div>
                <input type="hidden" name="data[id]" value="<?php echo $cat['id']; ?>">
                <input type="hidden" name="action" value="saveCategory">
                <button type="submit" class="btn btn-success" data-trigger="hover" data-toggle="popover" data-placement="bottom" data-container="body" data-content="Сохранить измения текста в данной категории?">Сохранить</button>
                <button type="submit" data-id="<?php echo $cat['id']; ?>" class="btn btn-warning deleteCategory">Удалить категорию</button>
            </form>

        </div>


    <?php endforeach; ?>
</div>
<script>
    $(".admin_file").change(function () {
        $(this).parent().parent().parent().parent().submit();
    });
    $(function () {
        $('[data-toggle="popover"]').popover()
    });
    $('.deleteCategory').click(function (e) {
        e.preventDefault();
        id = $(this).attr('data-id');
        $.post('/ajax',{'action':"delete", data:{'id':id,'table':'categories'}}, function (data) {
            location.reload();
        });
    });
    $('.addCategory').click(function (e) {
        e.preventDefault();
        $.post('/ajax',{'action':"addCategory"}, function (data) {
            location.reload();
        });
    });

</script>