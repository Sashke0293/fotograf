<?php

    $projects = DB::query("SELECT * FROM portfolio ORDER BY id DESC");

    $cats = DB::query('SELECT id,caption FROM categories');
?>

<div class="row">
    <div class="col-md-3 col-md-offset-9 text-right">
        <p></p>
        <button class="btn btn-success addProject">Добавить проект</button>
    </div>
</div>
<div class="row">
    <?php foreach($projects as $project): ?>
        <div class="col-md-6">
            <form action="/lazySubmit" method="post" enctype="multipart/form-data" class="well equal">
                <div class="media">
                    <div class="media-left">
                        <button type="button" class="clear-btn" onclick="$(this).find('.admin_file')[0].click();">
                            <img class="media-object" src="<?php echo $photoPath."portfolio/thumb/".$project['img']; ?>" width="300" alt="">
                            <input type="file" name="data[img]" class="admin_file hidden">
                        </button>
                    </div>
                    <div class="media-body">
                        Название <h4 class="media-heading"><input type="text" name="data[caption]" class="admin_input" value="<?php echo $project['caption']; ?>"></h4>
                        Категория 
                        <h5 class="media-heading">
                            <select name="data[category_id][]" class="admin_input" multiple>
                            <?php
                            foreach($cats as $v){
                                ?>
                                <option value="<?= $v['id'] ?>"<?php if(DB::queryFirstField('SELECT COUNT(*) FROM portfolio_categories WHERE category_id=%d AND portfolio_id=%d',$v['id'],$project['id'])) echo ' selected' ?>><?= $v['caption'] ?></option>
                                <?php
                            }
                            ?>
                            </select>
                        </h5>
                        Описание <textarea class="admin_textarea" name="data[description]" rows="4"><?php echo $project['description']; ?></textarea>
                        Длинное описание <textarea class="admin_textarea" name="data[description_full]" rows="4"><?php echo $project['description_full']; ?></textarea>
                        Алиас <input type="text" name="data[alias]" class="admin_input" value="<?php echo $project['alias']; ?>"><p></p>
                        В слайдере
                        <select name="data[slider]">
                            <option value="0"<?php if($project['slider']==0) echo " selected" ?>>Не показывать</option>
                            <option value="3"<?php if($project['slider']==3) echo " selected" ?>>Раньше всех</option>
                            <option value="2"<?php if($project['slider']==2) echo " selected" ?>>Вместе со всеми</option>
                            <option value="1"<?php if($project['slider']==1) echo " selected" ?>>После всех</option>
                        </select>
                    </div>
                </div>
                <input type="hidden" name="data[id]" value="<?php echo $project['id']; ?>">
                <input type="hidden" name="action" value="saveProject">
                <button type="submit" class="btn btn-success" data-trigger="hover" data-toggle="popover" data-placement="bottom" data-container="body" data-content="Сохранить измения текста в данном проекте?">Сохранить</button>
                <button type="button" class="btn btn-primary addPhotos" onclick="$(this).next().click();" title="Помощник" data-trigger="hover" data-toggle="popover" data-placement="bottom" data-container="body" data-content="Добавьте фотографий к вашему проекту. Рекомендуется добавлять не более 10 фотографий!">Добавить фотографий</button><input type="file" name="prgimg[]" multiple class="addPhotos_files hidden">
                <button type="submit" data-id="<?php echo $project['id']; ?>" class="btn btn-warning deleteProject">Удалить проект</button>

                <?php

                    $photos = DB::query("SELECT * FROM photo WHERE portfolio_id=%d",$project['id']);

                ?>
                <p></p>
                <div class="well">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <h4 class="header">
                                    Фотографии проекта
                                </h4>
                            </div>
                        </div>
                        <div class="row">
                            <?php foreach($photos as $photo): ?>
                                    <div class="col-md-3 drag-me">
                                        <a href="<?php echo $photoPath."photo/orig/".$photo['img']; ?>" data-id="<?php echo $photo['id']; ?>" class="fancy_project"><img src="<?php echo $photoPath."photo/thumb/".$photo['img']; ?>" width="100%"></a>
                                        <input placeholder="Alt" type="text" name="alt" style="width:100%;" value="<?= $photo['alt'] ?>">
                                        <input placeholder="Title" type="text" name="title" style="width:100%;" value="<?= $photo['title'] ?>">
                                        <button class="btn btn-primary updateAlt" style="width:100%;margin-bottom:10px;">Обновить</button>
                                        </form>
                                    </div>
                            <?php endforeach; ?>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <p></p>
                                <button type="button" class="btn btn-warning deletingImgs" title="Помощник" data-trigger="hover" data-toggle="popover" data-placement="top" data-container="body" data-content="Нажмите чтобы активировать удаление фотографий, нажмите ещё раз чтобы отключить эту функцию.">
                                    Удаление
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

        </div>


    <?php endforeach; ?>
</div>
<script>
    $(".admin_file").change(function () {
        $(this).parent().parent().parent().parent().submit();
    });
    $(".addPhotos_files").change(function () {
        $(this).parent().submit();
    });

    $(document).on('click','.deleter',function () {
        id = $(this).attr('data-id');
        var that = $(this);
        $.post('/ajax', {'action':'delete', 'data':{'id':id,'table':'photo'}}, function () {
            that.parent().remove();
        })
    });

    $('.deletingImgs').click(function () {
        if(!!$(document).find('.deleter').length){
            $(document).find('.deleter').remove();
        }else{
            $('.fancy_project').each(function () {
                id = $(this).attr('data-id');
                $(this).after('<button type="button" class="close deleter" data-id="'+id+'" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
            });
        }
    });
    $('.deleteProject').click(function (e) {
        e.preventDefault();
        id = $(this).attr('data-id');
        $.post('/ajax',{'action':"delete", data:{'id':id,'table':'portfolio'}}, function (data) {
            location.reload();
        });
    });
    $('.addProject').click(function (e) {
        e.preventDefault();
        $.post('/ajax',{'action':"addProject"}, function (data) {
            location.reload();
        });
    });
    $('.fancy_project').fancybox();
    $(function () {
        $('[data-toggle="popover"]').popover()
    });
    $('.drag-me').droppable({
        'accept':'.drag-me',
        'drop':function(event,ui){
            var cr = ui.helper;
            var dr = $(this);

            var id1=dr.find('.fancy_project').attr('data-id');
            var id2=cr.find('.fancy_project').attr('data-id');

            dr.find('.fancy_project').attr('data-id',id2);
            cr.find('.fancy_project').attr('data-id',id1);

            if(id2<id1){
                dr.after(cr);
            }else{
                dr.before(cr);
            }
            cr.css({'left':'','top':''});

            $.post('/ajax',{'action':"swapPhoto",'data':[id1,id2]}, function (data) {
                data=JSON.parse(data);
                console.log(data);
            });
        }
    });
    $('.drag-me').draggable({
        'containment':'parent',
        'revert':'invalid',
        'start':function(event,ui){
            ui.helper.css({'z-index':'9999'});
        },
        'stop':function(event,ui){
            ui.helper.css({'z-index':''});
        }
    });

    $('.updateAlt').click(function(e){
        var parent = $(this).parent();
        var id = parent.find('a').attr('data-id');
        var alt = parent.find('input[name=alt]').val();
        var title = parent.find('input[name=title]').val();

        $.post('/ajax',{
            'action':'updatePhotoAlt',
            'data':{
                'id':id,
                'alt':alt,
                'title':title
            }
        });

        e.preventDefault();
    });

</script>