<?php

    $abts = DB::query("SELECT * FROM images ORDER BY id ASC");
?>

<div class="row">
    <?php foreach($abts as $abt): ?>
        <div class="col-md-12">
            <form action="/lazySubmit" method="post" enctype="multipart/form-data" class="well equal">
                <div class="media">
                    <div class="media-left">
                        <button type="button" class="clear-btn" onclick="$(this).find('.admin_file')[0].click();">
                            <img class="media-object" src="<?php echo $imgPath."/".$abt['value']; ?>" width="300">
                            <input type="file" name="data[img]" class="admin_file hidden">
                        </button>
                    </div>
                    <div class="media-body">
                        <?= $abt['field'] ?>
                    </div>
                </div>
                <input type="hidden" name="data[id]" value="<?php echo $abt['id']; ?>">
                <input type="hidden" name="action" value="saveImages">
            </form>

        </div>


    <?php endforeach; ?>
</div>
<script>
    $(".admin_file").change(function () {
        $(this).parent().parent().parent().parent().submit();
    });
    $(function () {
        $('[data-toggle="popover"]').popover()
    });

</script>