
<div class="row">
    <div class="col-md-12">
        <ul class="nav nav-pills nav-justify sas-nav-admin">
            <?php /*<li role="presentation" class="<?php echo ($_SERVER['REQUEST_URI'] == "/super")? "active": ""; ?>"><a href="/super">Главная</a></li>*/ ?>
            <li role="presentation" class="<?php echo ($_SERVER['REQUEST_URI'] == "/super/categories")? "active": ""; ?>"><a href="/super/categories">Категории</a></li>
            <li role="presentation" class="<?php echo ($_SERVER['REQUEST_URI'] == "/super/projects")? "active": ""; ?>"><a href="/super/projects">Проекты</a></li>
            <li role="presentation" class="<?php echo ($_SERVER['REQUEST_URI'] == "/super/calendar")? "active": ""; ?>"><a href="/super/calendar">Календарь</a></li>
            <li role="presentation" class="<?php echo ($_SERVER['REQUEST_URI'] == "/super/fields")? "active": ""; ?>"><a href="/super/fields">Тексты</a></li>
            <li role="presentation" class="<?php echo ($_SERVER['REQUEST_URI'] == "/super/images")? "active": ""; ?>"><a href="/super/images">Картинки</a></li>
            <li role="presentation" class="<?php echo ($_SERVER['REQUEST_URI'] == "/super/price")? "active": ""; ?>"><a href="/super/price">Цены</a></li>
            <li role="presentation" class="<?php echo ($_SERVER['REQUEST_URI'] == "/super/about")? "active": ""; ?>"><a href="/super/about">О нас</a></li>
            <li role="presentation" class="<?php echo ($_SERVER['REQUEST_URI'] == "/super/settings")? "active": ""; ?>"><a href="/super/settings">Настройки</a></li>
        </ul>
    </div>
</div>