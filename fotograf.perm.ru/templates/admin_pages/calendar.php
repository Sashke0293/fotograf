
<div class="page-header">

    <div class="pull-right form-inline">
        <div class="btn-group">
            <button class="btn btn-primary" data-calendar-nav="prev"><< Туда</button>
            <button class="btn btn-default" data-calendar-nav="today">Сегодня</button>
            <button class="btn btn-primary" data-calendar-nav="next">Сюда >></button>
        </div>
        <div class="btn-group">
            <button class="btn btn-warning" data-calendar-view="year">Год</button>
            <button class="btn btn-warning active" data-calendar-view="month">Месяц</button>
            <button class="btn btn-warning" data-calendar-view="week">Неделя</button>
            <button class="btn btn-warning" data-calendar-view="day">День</button>
        </div>
    </div>

    <h3></h3>
</div>

<div id="calendar"></div>

<script type="text/javascript">
    (function($) {

        "use strict";

        var options = {
            events_source: "/assets/bootstrap/bower_components/bootstrap-calendar/events.json.php",
            view: 'month',
            tmpl_path: "/assets/bootstrap/bower_components/bootstrap-calendar/tmpls/",
            tmpl_cache: false,
            language: "ru-RU",
            onAfterEventsLoad: function(events) {
                if(!events) {
                    return;
                }
                var list = $('#eventlist');
                list.html('');

                $.each(events, function(key, val) {
                    $(document.createElement('li'))
                        .html('<a href="' + val.url + '">' + val.title + '</a>')
                        .appendTo(list);
                });
            },
            onAfterViewLoad: function(view) {
                $('.page-header h3').text(this.getTitle());
                $('.btn-group button').removeClass('active');
                $('button[data-calendar-view="' + view + '"]').addClass('active');

                var itsTime = [0,0];
                var itsTime_resizable = [0,0];
                var itsTime_draggable = [0,0];

//                function count_Time(){
//                    itsTime
//                }

                $(function() {
                    $( ".resizable" ).resizable({
                        grid: 30,
                        resize: function( event, ui ) {

                            var hours = Math.floor(ui.size.height/60);
                            var minutes = ui.size.height%60;

                            itsTime_resizable = hours*60 + minutes;

                            console.log(itsTime_resizable);

                        }
                    });
                    $( ".draggable" ).draggable({
                        grid: [ 30, 30 ],
                        create: function () {
                            itsTime = $('.cal-hours').text().split(' - ');
                            itsTime[0] = itsTime[0].split(':');
                            itsTime[1] = itsTime[1].split(':');
                            for (var itsT in itsTime[0]){
                                itsTime[0][itsT] = Number(itsTime[0][itsT]);
                            }
                            for (var itsT in itsTime[1]){
                                itsTime[1][itsT] = Number(itsTime[1][itsT]);
                            }
                            console.log(itsTime);
                        },
                        stop: function( event, ui ) {

                            if(ui.position.top<0){
                                var hours = Math.ceil(ui.position.top/60);
                            }else{
                                var hours = Math.floor(ui.position.top/60);
                            }
                            var minutes = ui.position.top%60;

                            itsTime_draggable = hours*60+minutes;

                            console.log(itsTime_draggable);

                        }
                    });
                });

            },
            classes: {
                months: {
                    general: 'label'
                }
            }
        };

        var calendar = $('#calendar').calendar(options);

        $('.btn-group button[data-calendar-nav]').each(function() {
            var $this = $(this);
            $this.click(function() {
                calendar.navigate($this.data('calendar-nav'));
            });
        });

        $('.btn-group button[data-calendar-view]').each(function() {
            var $this = $(this);
            $this.click(function() {
                calendar.view($this.data('calendar-view'));
            });
        });

        $('#first_day').change(function(){
            var value = $(this).val();
            value = value.length ? parseInt(value) : null;
            calendar.setOptions({first_day: value});
            calendar.view();
        });

        $('#language').change(function(){
            calendar.setLanguage($(this).val());
            calendar.view();
        });

        $('#events-in-modal').change(function(){
            var val = $(this).is(':checked') ? $(this).val() : null;
            calendar.setOptions({modal: val});
        });
        $('#events-modal .modal-header, #events-modal .modal-footer').click(function(e){
            //e.preventDefault();
            //e.stopPropagation();
        });
    }(jQuery));

    Date.prototype.addHours= function(h){
        this.setHours(this.getHours()+h);
        return this;
    }

    $(document).on('click', '.delEvent .close',function () {

        var id = $(this).parent().find('[data-event-id]').attr('data-event-id');
        var $that = $(this);
        $.post('/ajax',{'action':'delete','data':{'id':id,'table':'events'}}, function (data) {
            data = JSON.parse(data);
            if(data.status == 'success'){
                console.log(data);
                $that.parent().fadeOut(1000).promise().done(function () {
                    $(this).remove();
                });
            }
        });
    });

    $(document).on('click', '.lockDay',function (event) {
        var date = $(this).attr('data-date');

        $.post('/ajax',{'action':'addLockEvent','data':{'date':date,'part':false}}, function (data) {
            data = JSON.parse(data);
            if(data.success){
                console.log(data);
                $('[data-calendar-view="day"]').first().click();
            }
        });

        event.preventDefault();
    });
    $(document).on('click', '.lockDayPart',function (event) {
        var date = $(this).attr('data-date')+' '+prompt('Выберите время');
        var title = prompt('Комментарий');

        $.post('/ajax',{'action':'addLockEvent','data':{'date':date,'part':true,'title':title}}, function (data) {
            data = JSON.parse(data);
            if(data.success){
                console.log(data);
                $('[data-calendar-view="day"]').first().click();
            }
        });

        event.preventDefault();
    });
</script>