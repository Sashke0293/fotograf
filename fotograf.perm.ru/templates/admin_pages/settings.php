<div class="row">
    <form id="submitForm" action="/" method="post" class="col-md-4 col-md-offset-4">
        <p></p>
        <div class="input-group">
            <span class="input-group-addon" id="basic-addon1">Mail</span>
            <input name="auth[email]" type="email" class="form-control" value="<?php echo (isset($_SESSION['user']['email']))? $_SESSION['user']['email']: ""; ?>" placeholder="E-mail" aria-describedby="basic-addon1">
        </div>
        <p></p>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon2">Логин</span>
                        <input name="auth[login]" type="text" class="form-control" value="<?php echo (isset($_SESSION['user']['login']))? $_SESSION['user']['login']: "guest"; ?>" placeholder="Логин" aria-describedby="basic-addon2">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon3">Пароль</span>
                        <input name="auth[password]" type="password" class="form-control" value="<?php echo (isset($_SESSION['user']['password']))? $_SESSION['user']['password']: ""; ?>" placeholder="Пароль" aria-describedby="basic-addon3">
                    </div>
                </div>
            </div>
        </div>
        <p></p>
        <div class="btn-group btn-group-justified text-right" role="group" aria-label="Settings">
            <?php if($user->login): ?>
                <button type="button" class="btn btn-default btn-success wauto">Сохранить</button>
                <button class="btn btn-default btn-danger wauto" onclick="submitForm.reset();submitForm.submit();">Выйти</button>
            <? else: ?>
                    <button type="submit" class="btn btn-default btn-primary wauto">Войти</button>
            <? endif; ?>
        </div>
    </form>
</div>