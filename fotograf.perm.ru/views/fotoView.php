<?php

/**
 * Created by PhpStorm.
 * User: sasha
 * Date: 04.09.15
 * Time: 19:07
 */
class fotoView extends \Slim\View
{

    protected function render($template, $data = null)
    {
        $templatePathname = $this->getTemplatePathname($template);
        if (!is_file($templatePathname)) {
            throw new \RuntimeException("View cannot render `$template` because the template does not exist");
        }

        $data = array_merge($this->data->all(), (array) $data);
        extract($data);
        ob_start();
        require "fotograf.perm.ru/templates/header.php";
        require $templatePathname;
        require "fotograf.perm.ru/templates/footer.php";
        return ob_get_clean();
    }

    /**
     * Display template
     *
     * This method echoes the rendered template to the current output buffer
     *
     * @param  string   $template   Pathname of template file relative to templates directory
     * @param  array    $data       Any additonal data to be passed to the template.
     */
    public function displayRaw($template, $data = null)
    {
        echo $this->fetchRaw($template, $data);
    }

    /**
     * Return the contents of a rendered template file
     *
     * @param    string $template   The template pathname, relative to the template base directory
     * @param    array  $data       Any additonal data to be passed to the template.
     * @return string               The rendered template
     */
    public function fetchRaw($template, $data = null)
    {
        return $this->renderRaw($template, $data);
    }

    protected function renderRaw($template, $data = null)
    {
        $templatePathname = $this->getTemplatePathname($template);
        if (!is_file($templatePathname)) {
            throw new \RuntimeException("View cannot render `$template` because the template does not exist");
        }

        $data = array_merge($this->data->all(), (array) $data);
        extract($data);
        ob_start();
        require $templatePathname;
        return ob_get_clean();
    }

}