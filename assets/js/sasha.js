/**
 * Created by sasha on 04.09.15.
 */

equalheight = function(container){

    var currentTallest = 0,
        currentRowStart = 0,
        rowDivs = new Array(),
        $el,
        topPosition = 0;
    $(container).each(function() {

        $el = $(this);
        $($el).height('auto')
        topPostion = $el.position().top;

        if (currentRowStart != topPostion) {
            for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
                rowDivs[currentDiv].height(currentTallest);
            }
            rowDivs.length = 0; // empty the array
            currentRowStart = topPostion;
            currentTallest = $el.height();
            rowDivs.push($el);
        } else {
            rowDivs.push($el);
            currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
        }
        for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
            rowDivs[currentDiv].height(currentTallest);
        }
    });
}

$(window).load(function() {
    equalheight('.row .equal');
});


$(window).resize(function(){
    equalheight('.row .equal');
});



/* AjaxUploader.setState({

 step: с какой картинки грузить,
 container: куда грузить картинки,
 action: какую функцию ajax использовать
 debug: выводит в консоль инфу
 });
 *
 *
 *
 *
 * */
// AjaxUploader.run(); запустит загрузки изображений

//AjaxUploader = (function(){
//
//    var step = 0; // начало
//    var table = 'categories'; // начало
//    var count = 1; // кол-во загружаемых картинок больше 1 лучше пока не делать
//    var container = '.ajax_categories'; // контейнер в который загружаются картинки
//    var action = 'upload_imgs'; // функция которая будет вызвана в ajax
//    var add_data = {}; // дополнительные параметры для расширения
//    var debug = true;
//
//
//    function get_template(){
//        var tmpl = '';
//
//        return tmpl;
//    }
//
//    function upload_loop(step){
//
//        $.post('/ajax', {'action':action, 'data':{'step':step, 'count': count, 'table':table}}, function(data){
//
//            if(debug){
//                console.log(data);
//            }
//
//            data = JSON.parse(data);
//            if(data.stop){
//
//                $(container).append('' +
//                    '<div class="col-lg-4 col-md-4 col-sm-6">' +
//                    '<div class="project_img_container">' +
//                    '<img src="<?php echo $imgPath."galery/"; ?>'+data.img+'?refresh" class="project_img" alt=""> ' +
//                    "</div> " +
//                    "<a href='' class='project_hover'> " +
//                    "<span class='tbl'> <span class='tbl-cell'> " +
//                    "<h3 class='project_heading'>"+data.caption+" </h3> " +
//                    "<p class='project_description'>"+data.description+"</p> </span> </span> </a> </div>");
//
//                upload_loop(data.step);
//            }else{
//
//                var timer = 0;
//                $(".project_img_container").each(function () {
//                    $(this).delay(timer).promise().done(function () {
//                        $(this).fadeIn(500);
//                    });
//                    timer += 200;
//                });
//            }
//        });
//    }
//
//    return {
//        getState: function() {
//            console.log(step);
//            console.log(container);
//            console.log(action);
//            console.log(add_data);
//            console.log(debug);
//        },
//        setState: function(options) {
//            step = options.step || step;
//            container = options.container || container;
//            action = options.action || action;
//            add_data = options.add_data || add_data;
//            debug = options.debug || debug;
//            return true;
//        },
//        run: function() {
//            upload_loop(step);
//            return true;
//        }
//    };
//})();