/**
 * Created by sasha on 04.09.15.
 */

jQuery(document).ready(function($){
	var body = $('body');
	$('*').each(function(){
		var src=this.style.backgroundImage;
		if(src){
			var img=$(src.replace('url(','<img src=').replace(')','>'));
			img.hide();
			img.addClass('DeleteMe');
			img.appendTo(body);
		}
	});

	var imgs=$('img');

	var count =  imgs.length;
    
    var delta=96.5/count;
    var margin=0;

    var pdelta = 100/count;
    var pc = 0;

    var l_sh = $('.loader .sh');

    var l_pc = $('.loader .pc span');

    imgs.load(function(){
        margin+=delta;
        l_sh.css('margin-left',margin+'rem');

        pc+=pdelta;
        l_pc.html(Math.round(pc));
    });

    var w_e = function(event){
    	console.log('prevent');
    	event.preventDefault();
    }

    var s_e_t = false;
    var s_e = function(event){
    	console.log('timeout');
    	s_e_t = true;
    }
    setInterval(function(){
    	if(s_e_t){
    		$(document).scroll();
    		s_e_t = false;
    	}
    },300);

    var s = function(e){
    	$(window).off('mousewheel',w);
		$(document).off('scroll',s);
    	console.log('scroll');

		var c = $('.block.view');

		var delta = Math.floor($('.block').index(c) - ($(document).scrollTop() / window.innerHeight));
		if(!delta){
			$(window).one('mousewheel',w);
			$(document).one('scroll',s);
			return;
		}

		if(delta > 0){
			var n = c.prev('.block');
		}else{
			var n = c.next('.block');
		}

		if(n.length){
			/* Hover speed trick */
			$('.block').off('transitionend webkitTransitionEnd oTransitionEnd');
			$('.block.stop').removeClass('stop');
			
			n.one('transitionend webkitTransitionEnd oTransitionEnd',function(){
				n.addClass('stop');
			});
			/* End hover speed trick */

			n.addClass('view');
			c.removeClass('view');

			/* FF trick */
			$('#block-view').attr('id','');
			$('#block-next').attr('id','');
			$('#block-prev').attr('id','');
			/* End FF trick */

			n.attr('id','block-view');
			n.prev().attr('id','block-prev');
			n.next().attr('id','block-next');

			setTimeout(function(){
				$(window).one('mousewheel',w);
				$(document).one('scroll',s);
			},300);
		}else{
			$(window).one('mousewheel',w);
			$(document).one('scroll',s);
		}

		e.preventDefault();
	}
	var w = function(event,delta){
		$(window).off('mousewheel',w);
		$(document).off('scroll',s);
		console.log('wheel');
		if( $(event.target).closest('.scroll-normal').length ){
			$(window).one('mousewheel',w);
			$(document).one('scroll',s);
			return;
		}

		var c = $('.block.view');
		
		if(delta > 0){
			var n = c.prev('.block');
		}else{
			var n = c.next('.block');
		}

		if(n.length){
			/* Hover speed trick */
			$('.block').off('transitionend webkitTransitionEnd oTransitionEnd');
			$('.block.stop').removeClass('stop');
			
			n.one('transitionend webkitTransitionEnd oTransitionEnd',function(){
				n.addClass('stop');
			});
			/* End hover speed trick */

			n.addClass('view');
			c.removeClass('view');

			/* FF trick */
			$('#block-view').attr('id','');
			$('#block-next').attr('id','');
			$('#block-prev').attr('id','');

			n.attr('id','block-view');
			n.prev().attr('id','block-prev');
			n.next().attr('id','block-next');
			/* End FF trick */

			/* Scrollbar trick */
			$(document).scrollTop(window.innerHeight * $('.block').index(n));
			/* End Scrollbar trick */

			setTimeout(function(){
				$(window).one('mousewheel',w);
				$(document).one('scroll',s);
			},300);
		}else{
			$(window).one('mousewheel',w);
			$(document).one('scroll',s);
		}

		event.preventDefault();
	}

	var bind = false;
	var r = function(){
		if($('#block-view.scroll-normal').length){
			return;
		}
		if(!bind && window.innerWidth >= 1280){
			bind = true;
			$(window).on('mousewheel',w_e);
			$(document).on('scroll',s_e);
			$(window).one('mousewheel',w);
			$(document).one('scroll',s);
			$('html').css({'min-height':($('.block').length * window.innerHeight)});
		}else if(bind && window.innerWidth < 1280){
			bind = false;
			$(window).off('mousewheel');
			$(document).off('scroll');
			$('html').css({'min-height':''});
		}
	}

	$(document).on('click','.block.view + .block',function(e){
		$(window).off('mousewheel',w);
		$(document).off('scroll',s);
		console.log('click');

		var c = $('.block.view');
		var n = c.next('.block');

		/* Hover speed trick */
		$('.block').off('transitionend webkitTransitionEnd oTransitionEnd');
		$('.block.stop').removeClass('stop');

		n.one('transitionend webkitTransitionEnd oTransitionEnd',function(){
			n.addClass('stop');
		});
		/* End hover speed trick */

		n.addClass('view');
		c.removeClass('view');

		/* FF trick */
		$('#block-view').attr('id','');
		$('#block-next').attr('id','');
		$('#block-prev').attr('id','');

		n.attr('id','block-view');
		n.prev().attr('id','block-prev');
		n.next().attr('id','block-next');
		/* End FF trick */

		/* Scrollbar trick */
		$(document).scrollTop(window.innerHeight * $('.block').index(n));
		/* End Scrollbar trick */

		$(window).one('mousewheel',w);
		$(document).one('scroll',s);
	});

	$(window).resize(r);
	r();

	/* $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ */

	$('.mainslider li').click(function(event){
		var old = $('.mainslider li.view');
		var cur = $(this);

		var n = cur.next('li');
		var p = cur.prev('li');

		if(n.length){
			var nn = n.next();
			if(!nn.length){
				var nn = $('.mainslider li').first();
				n.after( nn );
			}
		}else{
			var nn = $('.mainslider li').first();
			cur.after( nn );
		}
		if(!p.length){
			var pp = $('.mainslider li').last();
			cur.before( pp );
		}

		old.removeClass('view');
		cur.addClass('view');

		var bg = cur.find('.bg').css('background-image');

		cur.parent().css('background-image',bg);
	});

	/* $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ */

	$(document).on('click','.maincalendar tr:nth-child(n+4) td:not(.day_na)',function(){
		var day = parseInt( $(this).html() );
		var d = new Date( parseInt( $('.maincalendar').attr('data-month') ) * 1000 );

		if( $(this).hasClass('day_p') ){
			d.setMonth( d.getMonth() - 1 );
		}else if( $(this).hasClass('day_n') ){
			d.setMonth( d.getMonth() + 1 );
		}

		d.setDate(day);

		var f = $('.mainform input#mainform-date , .priceform input#priceform-date');

		f.val( d.toLocaleString('ru-ru',{day:'numeric',month:'long'}) );
		f[0].disabled = true;

		$('.mainform input#mainform-date-unix , .priceform input#priceform-date-unix').val( d.getTime()/1000 );

		$('.maincalendar .day_s').removeClass('day_s');
		$(this).addClass('day_s');
	});

	var c = function(){
		var month = parseInt( $('.maincalendar').attr('data-month') );

		if($(this).hasClass('month_p')){
			month-=10*24*60*60;
		}else{
			month+=40*24*60*60;
		}
		$.post('/ajax',{
			action: 'getCalendar',
			data: month
		},function(data){
			var o = $('.maincalendar');
			var n = $(data);

			n.hide();

			n.insertBefore(o);

			o.fadeToggle().promise().done(function(){
				o.remove();
				$(document).one('click','.maincalendar .month_p , .maincalendar .month_n',c);
			});
			n.fadeToggle();
		});

	}

	$(document).one('click','.maincalendar .month_p , .maincalendar .month_n',c);

	$(document).on('click','.maincalendar .m_day_p , .maincalendar .m_day_n , .maincalendar .m_day_c',function(){
		var cur = $(this).html();
		var day = $('.maincalendar td[data-day='+cur+']');

		var days = $('.maincalendar td[data-day]');

		var di = days.index(day);

		if(di){
			var p = $(days[di-1]).html();
			$('.maincalendar .m_day_p').css({'visibility':''});
		}else{
			var p = '-';
			$('.maincalendar .m_day_p').css({'visibility':'hidden'});
		}

		if(di+1<days.length){
			var n = $(days[di+1]).html();
			$('.maincalendar .m_day_n').css({'visibility':''});
		}else{
			var n = '-';
			$('.maincalendar .m_day_n').css({'visibility':'hidden'});
		}

		var c = $(this).html();

		$('.maincalendar .m_day_p').html(p);
		$('.maincalendar .m_day_c').html(c);
		$('.maincalendar .m_day_n').html(n);


		var d = new Date( parseInt( $('.maincalendar').attr('data-month') ) * 1000 );

		d.setDate(c);

		var f = $('.mainform input#mainform-date , .priceform input#priceform-date');

		var months = ['января','февраля','марта','апреля','мая','июня','июля','августа','сентября','октября','ноября','декабря'];

		//f.val( d.toLocaleString('ru-RU',{day:'numeric',month:'long'}) ); /* Fuck FF on Android */
		f.val( d.getDate() + ' ' + months[ d.getMonth() ] );
		f[0].disabled = true;

		$('.mainform input#mainform-date-unix , .priceform input#priceform-date-unix').val( d.getTime()/1000 );
	});

	$('.mainform , .menuform , .priceform').ajaxForm(function(data){
		var r = JSON.parse(data);
		if(r.error){
			console.log(r.error);
			swal('Произошла ошибка','Проверьте введённые данные или повторите попытку позже','error');
		}else{
			var hide = $('#price-request-hide , #price-request-stage1');
			hide.each(function(k,v){
				v.checked = true;
			});

			swal('Ваша заявка принята','Наш ответ не придётся долго ждать','success');
		}
	});

	$('.menu-open').click(function(event){
		$('.menu').addClass('view');

		event.preventDefault();
	});
	$('.menu-close').click(function(event){
		$('.menu').removeClass('view');

		event.preventDefault();
	});

	if(!(window.innerWidth <= 719 || window.innerWidth < window.innerHeight)){
		$('.category-show').click(function(event){
			var id = $(this).attr('data-id');
			var div = $('#category'+id);
			if( !div.length ){
				div = $('<div class="leftblock scroll-normal" id="category'+id+'"><div></div></div>');

				$(document).on('click','.leftblock',function(event){
					div.removeClass('view');
					event.stopPropagation();
				});
				//div.find('div').append(cls);

				body.append(div);

				AjaxUploader.setState({
					table: 'portfolio',
					template: '/fotograf.perm.ru/helpers/tmpl/categories.php',
					container: '#category'+id+' > div',
					cols: [4,4,6],
					path: 'portfolio/thumb/',
					_field: 'category_id',
					_value: id
				});
				AjaxUploader.run();
			}

			setTimeout(function(){
				div.addClass('view');
			},100);

			event.preventDefault();
		});
	}

	$('.menuform #menuform-phone').mask('+7 (999) 999-99-99');
	$('.mainform #mainform-phone').mask('(999) 999-99-99');
	$('.mainform #mainform-date').mask('99.99.9999');
	$('.mainform #mainform-time').mask('99:99');
	$('.priceform #priceform-phone').mask('(999) 999-99-99');
	$('.priceform #priceform-date').mask('99.99.9999');
	$('.priceform #priceform-time').mask('99:99');

	//$('.mainform #mainform-time').timepicki();

	$('.mainform #mainform-time , .priceform #priceform-time').click(function(e){
		e.stopPropagation();

		var rez = this;
		var rv = rez.value.split(':');
		if(!rv[0] || rv[0][1]=='_'){
			rv[0]='12'
		}
		if(!rv[1] || rv[1][1]=='_'){
			rv[1]='00'
		}

		var timepicker = $('<div class="timepick usans-tcaps">'+
								'<div>'+
									'<a href="#">+1</a>'+
									'<input type="text" data-max="21" data-min="9">'+
									'<a href="#">-1</a>'+
								'</div>'+
								'<div>'+
									'<a href="#">+15</a>'+
									'<input type="text" data-max="45" data-min="0">'+
									'<a href="#">-15</a>'+
								'</div>'+
							'</div>');
		timepicker.hide();

		var inp = timepicker.find('input');
		inp.mask('99');
		inp[0].value=rv[0];
		inp[1].value=rv[1];

		inp.change(function(){
			var v=parseInt(this.value);
			var min=$(this).attr('data-min');
			var max=$(this).attr('data-max');

			if(v<min){
				v=min;
			}else if(v>max){
				v=max;
			}
			v+='';
			if(v.length<2){
				v='0'+v;
			}
			this.value=v;

			rez.value=inp[0].value+':'+inp[1].value;
		});

		var ars = timepicker.find('a');
		ars.click(function(e){
			e.preventDefault();

			var a = $(this);
			var t = a.parent().find('input');
			var v = parseInt(t[0].value);
			var delta = parseInt(a.html());
			t[0].value=v+delta;
			t.change();
		});

		$(this).before(timepicker);
		timepicker.fadeToggle();

		var f = function(event){
			if(!(timepicker[0] === event.target || $.contains(timepicker[0],event.target))){
				timepicker.fadeToggle().promise().done(function(){
					timepicker.remove();
				});
			}else{
				$(document).one('click',f)
			}
		}

		$(document).one('click',f);
	});
	
	/*if(!(window.innerWidth <= 719 || window.innerWidth < window.innerHeight)){
		$('.menu a[href^="/portfolio"]').click(function(e){
			var href = this.pathname;
			var a = $('.category-block a[href="'+href+'"]');

			if(a.length){
				a.click();
			}else{
				var cat = href.split('/')[2];
				window.location.href = "/#"+cat;
			}

			e.preventDefault();
		});

		if(window.location.hash !== ""){
			var cat = window.location.hash.replace('#','');
			var a = $('.category-block a[href$="'+cat+'"]');

			if(a.length){
				a.click();
			}else{
				alert(window.location.hash);
			}

			window.location.hash = "";
		}
	}*/

	var mx=false;
	$('.mainslider li').on('touchstart',function(e){
		mx = e.originalEvent.touches[0].clientX;
		$(this).addClass('hover');
		$('.mainslider').addClass('hover');
	});
	$('.mainslider li').on('touchmove',function(e){
		var old = $('.mainslider li.view');
		if(mx - 30 > e.originalEvent.changedTouches[0].clientX){
			var n = old.next();
		}else if(mx + 30 < e.originalEvent.changedTouches[0].clientX){
			var n = old.prev();
		}else{
			var n = old;
		}
		$('.mainslider li.hover').removeClass('hover');
		n.addClass('hover');
	});
	$(document).on('touchend',function(e){
		if(mx===false){
			return;
		}

		var old = $('.mainslider li.view');
		var cur = $('.mainslider li.hover');

		var n = cur.next('li');
		var p = cur.prev('li');

		if(n.length){
			var nn = n.next();
			if(!nn.length){
				var nn = $('.mainslider li').first();
				n.after( nn );
			}
		}else{
			var nn = $('.mainslider li').first();
			cur.after( nn );
		}
		if(!p.length){
			var pp = $('.mainslider li').last();
			cur.before( pp );
		}

		old.removeClass('view');
		cur.addClass('view');
		cur.removeClass('hover');

		var bg = cur.find('.bg').css('background-image');

		cur.parent().css('background-image',bg).removeClass('hover');

		mx=false;
	});


});

$(window).load(function() {
    $('.DeleteMe').remove();
    setTimeout(function(){
    	$('.loader').fadeToggle().promise().done(function(){
	    	$('.loader').remove();
	    });
    },1000);
});