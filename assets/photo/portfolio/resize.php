<?php
 
// This script takes an image and resizes it to the given dimensions, then saves
// that version on the filesystem so Apache can serve it directly in the future.
 
// It is inspired by Drupal's ImageCache module [1] and a blog post by Sumit
// Birla [2], but was written from scratch.
 
// It automatically doubles the dimensions if the suffix '@2x' is used, for use
// with the jQuery Retina Display plugin or retina.js [4].
 
// [1]: http://drupal.org/project/imagecache
// [2]: http://sumitbirla.com/2011/11/how-to-build-a-scalable-caching-resizing-image-server/
// [3]: https://github.com/mcilvena/jQuery-Retina-Display-Plugin
// [4]: http://retinajs.com/
 
// https://gist.github.com/davejamesmiller/3236415
 
chdir(dirname(__FILE__));
 
$size = $_GET['size'];
$file = $_GET['file'];
 
$original = "orig/$file";
$target = "$size/$file";
 
// Check the size is valid

$thumb_size = array(
  'thumb' => array(640,null)
);

if(array_key_exists($size,$thumb_size)){
	$thumbWidth = $thumb_size[$size][0];
  $thumbHeight = $thumb_size[$size][1];
}else{
	die('[3,"invalid size"]');
}
 
// Check the filename is safe & check file type
if (preg_match('#\.(jpg|jpeg|png|gif)$#i', $file, $matches) && strpos($file, '..') === false) {
  $extension = $matches[1];
} else {
  die('[3,"invalid filename"]');
}

// Check the original file exists
if (!is_file($original)) {
  die('[201,"missing file"]');
}
 
// Make sure the directory exists
if (!is_dir($size)) {
  mkdir($size);
  if (!is_dir($size)) {
    die('[200,"cannot create directory"]');
  }
  chmod($size, 0777);
}
 
// Make sure the file doesn't exist already
if (!file_exists($target)) {
 
  // Make sure we have enough memory
  ini_set('memory_limit', 128*1024*1024);
 
  // Get the current size & file type
  list($width, $height, $type) = getimagesize($original);
 
  // Load the image
  switch ($type) {
    case IMAGETYPE_GIF:
      $image = imagecreatefromgif($original);
      break;
 
    case IMAGETYPE_JPEG:
      $image = imagecreatefromjpeg($original);
      break;
 
    case IMAGETYPE_PNG:
      $image = imagecreatefrompng($original);
      break;
 
    default:
      die('[202,"invalid file type"]');
  }
 
  // Calculate height automatically if not given
  if ($thumbHeight === null) {
    $thumbHeight = round($height * $thumbWidth / $width);
  }
 
 // Calculate width automatically if not given
  if ($thumbWidth === null) {
    $thumbWidth = round($width * $thumbHeight / $height);
  }
  
  //Check size
  if($thumbWidth === null){
	  die('[300,"incorrect site env - size"]');
  }
 
  // Ratio to resize by
  $widthProportion = $thumbWidth / $width;
  $heightProportion = $thumbHeight / $height;
  $proportion = max($widthProportion, $heightProportion);
 
  // Area of original image that will be used
  $origWidth = floor($thumbWidth / $proportion);
  $origHeight = floor($thumbHeight / $proportion);
 
  // Co-ordinates of original image to use
  $x1 = floor($width - $origWidth) / 2;
  $y1 = floor($height - $origHeight) / 2;
 
  // Resize the image
  $thumbImage = imagecreatetruecolor($thumbWidth, $thumbHeight);
  imagecopyresampled($thumbImage, $image, 0, 0, $x1, $y1, $thumbWidth, $thumbHeight, $origWidth, $origHeight);
 
  // Save the new image
  switch ($type)
  {
    case IMAGETYPE_GIF:
      imagegif($thumbImage, $target);
      break;
 
    case IMAGETYPE_JPEG:
      imagejpeg($thumbImage, $target, 90);
      break;
 
    case IMAGETYPE_PNG:
      imagepng($thumbImage, $target);
      break;
 
    default:
      throw new LogicException;
  }
 
  // Make sure it's writable
  chmod($target, 0666);
 
  // Close the files
  imagedestroy($image);
  imagedestroy($thumbImage);
}
 
// Send the file header
$data = getimagesize($original);
if (!$data) {
  die('[203,"cannot get mimetype"]');
} else {
  header('Content-Type: ' . $data['mime']);
}
 
// Send the file to the browser
readfile($target);
